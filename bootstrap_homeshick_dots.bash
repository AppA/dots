#!/usr/bin/env bash

# Bootstrapping script for dotfiles. Kinda hacky tho.
# Usage: ./<reponame>/bootstrap_homeshick_dots.bash [--gui] <reponame>

if ! command -v git >/dev/null ; then echo "ERR: git not found!" ; exit 1 ; fi
#if ! command -v curl >/dev/null ; then echo "ERR: curl not found!" ; exit 1 ; fi

while true ; do
	case $1 in
		--gui)
			unset guirepo ; guirepo="https://gitlab.com/AppA/guidots"
			shift
			;;
		*)	
			unset repo ; repo="$1"
			if [ ! -d "$repo/.git/" ] ; then echo "ERR: Specified directory is not a git repo!" ; exit 1 ; fi
			break
			;;
	esac
done


# Install homeshick
git clone --depth=1 https://github.com/andsens/homeshick ~/.homesick/repos/homeshick
source ~/.homesick/repos/homeshick/homeshick.sh

# Move repo to ~/.homesick/repos
mv "$repo" ~/.homesick/repos/

# cd into the repo and initialize all submodules to the commit which is registered in the parent repo.
cd ~/.homesick/repos/"$repo" &&
git submodule init && git submodule update --init --recursive &&
cd -

# Optionally install GUI-specific dotfiles.
if [ -n "$guirepo" ] ;
	then 
		cd ~/.homesick/repos/
		git clone --depth=1 "$guirepo" 
		cd -
fi

# Link it all to ~ 
homeshick link --force 
