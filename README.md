# README #

My main dotfiles.

* Originating from Ubuntu 12.04
* Managed using [homeshick](https://github.com/andsens/homeshick).

### What is this repository for? ###

Managing my dotfiles.

### How do I get set up? ###

1. `git clone https://gitlab.com/AppA/dots`
2. `./dots/bootstrap_homeshick_dots.bash [--gui] dots`
	* The `--gui` flag pulls in https://gitlab.com/AppA/guidots (which is usually unnecessary for headless machines)


### How to push changes (to GitLab) ?
1. Make sure the GitLab SSH key is added and working by testing using `ssh -T git@gitlab.example.com`
2. `git remote set-url origin ssh://git@gitlab.com/appa/dots` ([more info](https://stackoverflow.com/questions/47125916/why-is-gitlab-asking-me-to-enter-my-user-credentials))

---


### `tmux`

To enable missing tmux plugins: Within tmux, do `<prefix> + I` (that's a capital I!) - [More info about `tpm`](https://github.com/tmux-plugins/tpm)  
Some plugins are managed as submodules, others (like the hueg tmux-fingers) are better left managed by TPM.
