setlocal textwidth=180
wincmd L
vertical resize 80
normal ze

" Simplify help navigation
" The following mappings simplify navigation when viewing help:
" * Press Enter to jump to the subject (topic) under the cursor.
" * Press Backspace to return from the last jump.
" * Press s to find the next subject, or S to find the previous subject.
" * Press o to find the next option, or O to find the previous option.
" http://vim.wikia.com/wiki/Learn_to_use_help#Simplify_help_navigation
nnoremap <buffer> <CR> <C-]>
nnoremap <buffer> <BS> <C-T>
nnoremap <buffer> o /'\l\{2,\}'<CR>
nnoremap <buffer> O ?'\l\{2,\}'<CR>
nnoremap <buffer> s /\|\zs\S\+\ze\|<CR>
nnoremap <buffer> S ?\|\zs\S\+\ze\|<CR>

" TODO: Only allow Ctrl+LeftMouse to follow tags in vim's help files.
" http://vimhelp.appspot.com/term.txt.html#double-click
"noremap <buffer> <2-LeftMouse> <C-LeftMouse> 
"noremap <buffer> <3-LeftMouse> <C-LeftMouse> 
