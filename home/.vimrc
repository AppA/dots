""""""""""
" .vimrc "
""""""""""

" Thanks:
" I probably couldn't have made Vim the way I wanted if it weren't for #vim @ Freenode,
" Vim Stack Exchange, /r/vim and of course the rest of Vim on the Interwebz.


""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-plug (https://github.com/junegunn/vim-plug)

" Install declared plugins -> :PlugInstall
" Making changes (like deleting plugins or pinning them to a commit):
" 1. Make changes to plugin declaration below.
" 2. Reload vimrc (:source ~/.vimrc)
" 3. :PlugUpdate / :PlugClean
" Plugins can be found in ~/.vim/plugged/
call plug#begin()

Plug 'pearofducks/ansible-vim', { 'commit': 'bc9c3bf48961f5babb24243bd407c715ce551210' }
Plug 'vim-airline/vim-airline', { 'commit': '30f8ada1d6021d89228092b3c51840916c75a542' }
Plug 'plasticboy/vim-markdown', { 'commit': '8e5d86f7b85234d3d1b4207dceebc43a768ed5d4' }
Plug 'SirVer/ultisnips'

call plug#end()
""""""""""""""""""""""""""""""""""""""""""""""""""


" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
"runtime! debian.vim

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set nocompatible

" Fix Ctrl+w and Backspace behavior
" Which is weird, since I didn't have this problem in v8.0, but suddenly started experiencing Ctrl+w and backspace breaking (after going into insert mode and back) in v8.2
" Docs: http://vimhelp.appspot.com/options.txt.html#%27bs%27
" Explanation: https://stackoverflow.com/questions/18777705/vim-whats-the-default-backspace-behavior
set backspace=indent,eol,start

" Filetype - http://vimhelp.appspot.com/filetype.txt.html
filetype plugin indent on

" Set cryptmethod to blowfish2. See ':h encryption' and ':h cryptmethod'.
" WARNING: Does not work on Vim <7.4.401
set cryptmethod=blowfish2

" Prevent vim from modifying my Xorg primary buffer: https://stackoverflow.com/a/38622567
" Default value was: clipboard=autoselect,exclude:cons\|linux
set clipboard=



"""""""""""""""""
" < COSMETICS > "
"""""""""""""""""

""" Color management
" vim can autodetect this based on $TERM (e.g. 'xterm-256color')
" but it can be set to force 256 colors
" set t_Co=256
if &t_Co < 256
    colorscheme default
    "set nocursorline " looks bad in this mode
else
    set background=dark
    " let g:solarized_termcolors=256 " instead of 16 color with mapping in terminal
    colorscheme wombat256i_mod
    " customized colors
    " highlight SignColumn ctermbg=234
    " highlight StatusLine cterm=bold ctermfg=245 ctermbg=235
    " highlight StatusLineNC cterm=bold ctermfg=245 ctermbg=235
endif

""" Color overrides

" Enable cursorline in insert mode.
autocmd InsertEnter * set cul
autocmd InsertLeave * set nocul

" Set cursor column color
highlight CursorColumn ctermbg=230 guibg=#ffffd7

" Instead of red background when spellchecking, do underlines.
" Source - https://github.com/mgedmin/dotvim/blob/master/vimrc#L1270-L1271
highlight SpellBad              cterm=underline ctermfg=red ctermbg=NONE
highlight SpellCap              cterm=underline ctermfg=blue ctermbg=NONE


"""

" Vim5 and later versions support syntax highlighting. Uncommenting the next line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

set background=dark	" If using a dark background within the editing area and syntax highlighting turn on this option as well
set number	" Turn line numbers on
set numberwidth=1	" For Vim v7 or greater, this changes the width of the "gutter" column used for displaying line numbers.
set ruler	" Show the cursor position all the time
set showcmd	" Show the commands which you enter in the file - https://www.hscripts.com/tutorials/vim-commands/showcmd.html

" When on, splitting a window will put the new window below the current one.
" http://vimhelp.appspot.com/options.txt.html#%27splitbelow%27
set splitbelow

set wrap " Set autowrapping of text
set linebreak " Configure autowrapping to wrap after spaces instead of in the middle of words.

" Highlight searched text
" Do :nohl to turn it off (for that session).
set hlsearch

" Search as you type.
" Vim will start searching when you type the first character of the search string. As you type in more characters, the search is refined.
set incsearch

" wildmenu / wildmode -
" http://stackoverflow.com/questions/9511253/how-to-effectively-use-vim-wildmenu
set wildmenu
set wildmode=longest:full,full

" Interpret .md files as markdown instead of modula2
" More info - http://stackoverflow.com/a/14779012 and http://stackoverflow.com/a/23279293
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown


set laststatus=2	" Set this to use (custom) statuslines.

""""""""""
" airline
""""""""""
let g:airline_theme = 'dark'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#syntastic#enabled = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#wordcount#enabled = 0  " Disable wordcount. Instead do g+Ctrl+g

"set timeoutlen=50 " https://github.com/bling/vim-airline/wiki/FAQ#there-is-a-pause-when-leaving-insert-mode
" Tweak ttimeout because it messes with the time it takes to succesfully do a keycombo.
set ttimeout
set ttimeoutlen=50

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

"" Obsolete (somewhat, kept just in case)
"" unicode symbols
"let g:airline_left_sep = '»'
"let g:airline_left_sep = '▶'
"let g:airline_right_sep = '«'
"let g:airline_right_sep = '◀'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.linenr = '¶'
"let g:airline_symbols.branch = '⎇'
"let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
"let g:airline_symbols.whitespace = 'Ξ'

" non-unicode symbols
let g:airline_left_sep = '>'
let g:airline_right_sep = '<'
let g:airline_symbols.linenr = 'L#'
let g:airline_symbols.branch = 'br'
let g:airline_symbols.paste = '[PASTE]'
let g:airline_symbols.whitespace = 'ws'

" variable names, see :help airline (airline-customization) for more info
"let g:airline_section_a
"let g:airline_section_b
let g:airline_section_c = '%F'  " Displays file path.
"let g:airline_section_gutter
"let g:airline_section_x
"let g:airline_section_y
"let g:airline_section_z
" Obsolete. If you want to view wordcount then g+Ctrl+g
"let g:airline_section_warning = '%{WordCount()} wds'

""""""""""""""""""""

" This makes new window splits (using :vsp for example) open at the right side
" and new window at the bottom.
" See this article for an illustration:
" https://technotales.wordpress.com/2010/04/29/vim-splits-a-guide-to-doing-exactly-what-you-want/
set splitbelow
set splitright

""""""""""""""""""
" </ COSMETICS > "
""""""""""""""""""



" Obsolete (for now)
"""" vim-lexical plugin (obsolete (for now))
"""" https://github.com/reedes/vim-lexical
"
"" Define which filetypes are going to get lexical applied.
"augroup lexical
"  autocmd!
"  autocmd FileType markdown,mkd call lexical#init()
"  autocmd FileType textile call lexical#init()
"  autocmd FileType text call lexical#init({ 'spell': 0 })
"augroup END

"" Location to the thesaurus. Also for vim.
"" Online location is http://www.gutenberg.org/files/3202/ - but file is around ~25mb
"let g:lexical#thesaurus = ['~/.thesaurus/mthesaur.txt',]
"set thesaurus+=~/.thesaurus/mthesaur.txt



""" ultisnips plugin
""" https://github.com/SirVer/ultisnips
""" Also checkout these pre-made snippets - https://github.com/honza/vim-snippets

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Temporary workaround, see https://github.com/SirVer/ultisnips/issues/711#issuecomment-227159748
let g:UltiSnipsSnippetDirectories = ['~/.vim/UltiSnips', 'UltiSnips']


""" tagbar plugin
""" https://github.com/majutsushi/tagbar/

" Ansible specific see https://github.com/majutsushi/tagbar/wiki#ansible
let g:tagbar_type_ansible = {
    \ 'ctagstype' : 'ansible',
    \ 'kinds' : [
        \ 't:tasks'
    \ ],
    \ 'sort' : 0
\ }



""" Autotocommands

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Makes gf go into Ansible's role dirs.
" https://github.com/mgedmin/dotvim/commit/6de668a70cd26f877824d112dce4f5394fdcd4fd
augroup Ansible
  autocmd!
  autocmd BufReadPre,BufNewFile */roles/*/*.yml
              \ let &l:path = expand("%:p:h:h")."/**,".&g:path
augroup END

""" / Autocommands


" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"if has("autocmd")
"  filetype plugin indent on
"endif

" TAB settings. More info:
" https://old.reddit.com/r/vim/comments/99ylz8/confused_about_the_difference_between_tabstop_and/
" https://medium.com/@arisweedler/tab-settings-in-vim-1ea0863c5990
" http://tedlogan.com/techblog3.html
" https://www.youtube.com/watch?v=6sslhOEd2ZA
set tabstop=2  "How many charactor blocks a TAB character displays as (this value might be different across different text display systems, so keep that in mind)
set shiftwidth=2  "How many charactor blocks
set noexpandtab  "Do not use spaces when hitting TAB
" Also, I'm trying to use tabs instead of spaces because: https://news.ycombinator.com/item?id=20381027


""" Custom Mappings

"" Leader mappings
" Set the leader in vim
" http://usevim.com/2012/07/20/vim101-leader/

" noremap vs. map:
" consider this example: 'nnoremap dd jdd' vs. 'nmap dd jdd':
" 1st one: move down one line and dd
" 2nd one: move down one line and .. oh that's a map .. so move down one line and .. oh that's a map .. so move down one line (infinite recursion)
" For example: If you have  nnoremap dd :if CheckSomething() | execute 'normal! dd' | endif<cr>  and you want to use that 'dd' instead of vanilla vim's 'dd' inside of this map:  nmap <leader>d "+dd   *then* nmap would be appropriate, but usually that's not the case.
" As a general rule: Use nmap when you want to (a) use  nmap (whatever) <plug>(whatever)  (b) want to use  nmap (whatever) (another map)
let mapleader='\'

map <leader>sp :set spell!<cr> " Toggle spellcheck
"map <leader>h :nohl<cr> " Toggle search highlighting
map <leader>p :set paste!<cr> " Toggle paste mode
xnoremap <leader>y "+y  " Yank selection into Ctrl+v buffer. We use xnoremap because visual selection mode is different from normal mode.
nmap <leader>v 0v$hh " Visually select line, minus the line break / newline.
"nmap <leader>yc 0v$hc " Yank line as string and enter correction mode. "" Uh... why is that not working as intended?
nmap <leader>cl /http<cr>vi("+y:nohl<cr> " Quick link copy in md files.
map <leader>l :set list!<cr> " Toggle listing of hidden characters such as tabs and EOLs. See http://vim.wikia.com/wiki/Highlight_unwanted_spaces#Using_the_list_and_listchars_options
" Insert timestamp. Includes ISO 8601 format at the start of string.
nmap <leader>ts :read !echo -n "$(date \+\%F__\%T\%z__\%a)"<cr>kJE
nmap <leader>' k :read !sh ~/.local/bin/myscripts/hnlink2mdlink.sh 

" Go to next / previous buffer. Handy for when gf into file.
nmap <leader>bn :bnext<cr>
nmap <leader>bp :bprevious<cr>
nmap <leader>be :blast<cr>

"" / Leader mappings


" Remap Ctrl+g to double Esc. Double because otherwise hitting C-g will wait the length of timeoutlen before executing. Remapped to C-g to have some Emacs (EEK!) compatibility :)
" More info - http://stackoverflow.com/a/80761
"inoremap <C-c> <Esc><Esc>  " Previously was Ctrl+c
imap <C-c> <Nop>
inoremap <C-g> <Esc><Esc>
" Also, do consider that this doesn't work in paste mode. See https://vimhelp.org/options.txt.html#%27paste%27

" Remap Ctrl-a and Ctrl-e in insert mode to move to line beginning and line to the end of the line respectively.
" This coincides with *gasp* the Emacs mode in Bash readline, and I'm pretty used to that mapping.
" See this SO post about it - http://stackoverflow.com/a/11487877
" You do lose some features, but I don't think I'll be needing them at this moment.
inoremap <C-e> <End>
inoremap <C-a> <Home>

" Map Ctrl+up and Ctrl+down to C-e and C-y in normal mode
nmap <C-Up> <C-y>
nmap <C-Down> <C-e>

" Map Alt + Left/Right for switching between tabs.
noremap <A-Left> gT
noremap <A-Right> gt


"""" Macros
" View macros with :registers or view macro under @t with :reg t
" To insert already recorded macro for @t: In insert mode: C-r C-r t
let @c = 'ggVG"+Y``' " Copies entire buffer into Ctrl+v buffer.
let @s = ':s/\%V /_/g' " Replaces all spaces in visual selection with underscores.

" The @l macro is used to transition old link notation to markdown notation.
" Though it originally contained single-byte chars.
"let @l = '0r*kra[@7]Jr(A)'  " This doesn't work if you specify it like this.
"They need to be either:
" 1. Escaped
"let @l = '0r*\xkra[\x@7]Jr(A)'  " Turn old notation to markdown link (escaped)
" 2. Use explicit keycodes (needed to rewrite it by hand)
let @l = '0r*la[$a](JxA)<Ctrl-c>'
" 3: convert back using iconv()
"let @l = iconv("0r*krkri[@7](JkDA)", 'utf-8', 'latin1')
" Thanks to Houl @ #vim (Freenode)

" Copy markdown bulletpoint to the next window.
let @m = 'ddp'
"""" / Macros


" Scrollwheel behavior modification in normal mode
" http://superuser.com/questions/351972/how-can-i-change-the-scroll-wheel-behavior-in-vim-so-that-it-scrolls-instead-of
"set mouse=a
" TODO: Figure out a way to disable automatic hyperlink following upon clicking hyperlink in help files.
" Alternative method: http://stackoverflow.com/a/12201974


" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showmatch		" Show matching brackets when text indicator is over them.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching, make searches case-sensitive only if they contain upper-case characters.
"set autowrite		" Automatically save before commands like :next and :make
"set hidden             " Hide buffers when they are abandoned
set history=9001	" We gon way back...


" Language-dependant autocompletion (otherwise known as Intellisense for Visual Studio)
" Source - http://vim.wikia.com/wiki/Omni_completion
" To use omni completion, type <C-X><C-O> while open in Insert mode. If matching names are found, a pop-up menu opens which can be navigated using the <C-N> and <C-P> keys.
"filetype plugin on  " <-- Already enabled elsewhere.
set omnifunc=syntaxcomplete#Complete


" Set this to get access to lots of tabs.
set tabpagemax=20

" Tell vim not to add a newline on every (saved) file.
" http://stackoverflow.com/questions/14171254/why-would-vim-add-a-new-line-at-the-end-of-a-file
" set fileformats+=dos
" But apparently there's no easy way to do this -_-

" This prevents some vulnerabilities. See
" http://www.techrepublic.com/blog/it-security/turn-off-modeline-support-in-vim/
:set modelines=0
:set nomodeline
