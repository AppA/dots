#!/usr/bin/env sh

# A wrapper for getting / setting (available) CPU governors and frequencies.
# Most of this is pretty Linux-specific (for now)

f__print_help () {
cat << EOF
Usage: $(basename $0) [-m|--mega] [-l|--list] [[-g|--governor] <value>] [--min <value>] [--max <value>] [--minmax <value>] [-h|--help]
Running $(basename $0) with -l or --list prints information in the following format:
CPU number - Available CPU governors | Current CPU governor | Minimum current frequency (Minimum allowed frequency) | Maximum current frequency (Maximum allowed frequency)

Note: Provide the -m (or --mega) flag before the -l (or --list) flag if you want the units to be printed in MHz.

-l, --list              Print information.
-g, --governor <value>  Set desired CPU governor.
-m, --mega              Print and input units in MHz instead of GHz.
--min <value>           Set the desired minimum CPU frequency.
--max <value>           Set the desired maximum CPU frequency.
--minmax <value>        Set the desired minimum and maximum CPU frequency to the same value.
-h, --help              Print this help message.
EOF
}

if [ -z "$1" ] ; then f__print_help && exit 1 ; fi

# Default unit denomination is in GHz.
scaling_operand="1000000"
denomination="GHz"

while true ; do
	case $1 in
		-m|--mega)
			scaling_operand="1000"  # MHz
			denomination="MHz"
			shift ;;
		-l|--list) justlist=1 ; break ;;
		-g|--governor)
			if [ -n "$2" ]
				then desired_governor="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf  "ERR: Must specify the desired governor! Run this script without arguments to see available governors." ; exit 1
			fi ;;
		--min)
			if [ -n "$2" ]
				then desired_min_freq="$2"
				shift 2
				else printf  "ERR: Must specify the minimum frequency!" ; exit 1
			fi ;;
		--max)
			if [ -n "$2" ]
				then desired_max_freq="$2"
				shift 2
				else printf  "ERR: Must specify the maximum frequency!" ; exit 1
			fi ;;
		--minmax)
			if [ -n "$2" ]
				then desired_min_freq="$2" ; desired_max_freq="$2"
				shift 2
				else printf  "ERR: Must specify the minimum+maximum frequency!" ; exit 1
			fi ;;
		-h|--help) f__print_help ; exit 0 ;;
		*) break ;;
	esac
done

desired_max_freq_f="$(awk -v max=$(echo $desired_max_freq) -v so=$scaling_operand 'BEGIN {printf "%.f\n", max * so}')"  # Convert the frequency value in argument to a value that's passable to $scaling_max_freq_path
desired_min_freq_f="$(awk -v min=$(echo $desired_min_freq) -v so=$scaling_operand 'BEGIN {printf "%.f\n", min * so}')"  # Convert the frequency value in argument to a value that's passable to $scaling_min_freq_path

for cpu in /sys/devices/system/cpu/cpu[0-999999999]*
do
	cpu_dir="$(dirname $cpu)"  # Get the path to the CPU directory.
	cpu_num="$(basename $cpu)"  # Extract CPU number.
	scaling_min_freq_path="$cpu_dir/$cpu_num/cpufreq/scaling_min_freq"  # Path where minimum frequency is stored.
	scaling_max_freq_path="$cpu_dir/$cpu_num/cpufreq/scaling_max_freq"  # Path where maximum frequency is stored.
	cpuinfo_min_freq_path="$cpu_dir/$cpu_num/cpufreq/cpuinfo_min_freq"  # Path where minimum allowed frequency is stored.
	cpuinfo_max_freq_path="$cpu_dir/$cpu_num/cpufreq/cpuinfo_max_freq"  # Path where maximum allowed frequency is stored.
	scaling_gov_path="$cpu_dir/$cpu_num/cpufreq/scaling_governor"  # Path where current governor value is stored.
	current_gov="$(cat $scaling_gov_path)"  # Get current governor.
	available_govs="$(cat $cpu_dir/$cpu_num/cpufreq/scaling_available_governors)"  # Get available governors
	
	# Conversions to human-readable units (using awk because not all systems might have bc installed)
	current_minfreq_h="$(awk -v min=$(cat $scaling_min_freq_path) -v so=$scaling_operand 'BEGIN {printf "%.1f\n", min / so}')"
	current_maxfreq_h="$(awk -v max=$(cat $scaling_max_freq_path) -v so=$scaling_operand 'BEGIN {printf "%.1f\n", max / so}')"
	allowed_minfreq_h="$(awk -v min=$(cat $cpuinfo_min_freq_path) -v so=$scaling_operand 'BEGIN {printf "%.1f\n", min / so}')"
	allowed_maxfreq_h="$(awk -v max=$(cat $cpuinfo_max_freq_path) -v so=$scaling_operand 'BEGIN {printf "%.1f\n", max / so}')"
	
	# Command for -l or --list.
	[ -n "$justlist" ] && echo "$cpu_num - Avail.: $available_govs | Curr.: $current_gov | Min: $current_minfreq_h $denomination ($allowed_minfreq_h) | Max: $current_maxfreq_h $denomination ($allowed_maxfreq_h)" 
	
	# Now for the stuff that requires root privs:
	if [ -n "$desired_governor" ] 
		then $privprefix /bin/sh -c "echo $desired_governor > $scaling_gov_path" && echo "$cpu_num: Governor set to $desired_governor"  # Set the governor.
	fi
	if [ "$desired_min_freq_f" -ne 0 ]
		then $privprefix /bin/sh -c "echo $desired_min_freq_f > $scaling_min_freq_path" && echo "$cpu_num: Minimum frequency set to $desired_min_freq $denomination"  # Set minimum frequency.
	fi
	if [ "$desired_max_freq_f" -ne 0  ]
		then $privprefix /bin/sh -c "echo $desired_max_freq_f > $scaling_max_freq_path" && echo "$cpu_num: Maximum frequency set to $desired_max_freq $denomination"  # Set maximum frequency.
	fi
done
