#!/usr/bin/env sh

# This script posts messages to a Matrix room. You can optionally specify a GPG pubkey so that your message gets encrypted before sending.
# This script can be used (non-)interactively.

# Usage (IN EXACTLY THAT FLAG ORDER! Or not. I'll show you down below.)
# ./post_matrix_msg.sh [-g|--gpg-pubkey gpg_public_key] [-u|--unformatted unformatted_text] [-i|--info] [-e|--encrypt text_to_encrypt] -l|--link URL
# -i flag posts useful info (see f__show_useful_info) in PGP ciphertext.
# Non-interactive usage: echo "something" | ./post_matrix_msg.sh [-g|--gpg-pubkey] gpg_public_key]] [-e|--encrypt] -l|--link URL
# Examples:
# echo "Encrypt this text." | ./post_matrix_msg.sh --gpg-pubkey my_gpg_pubkey.pub --encrypt --unformatted "This ends up being unformatted text." --link https://matrix.org/
# echo "This ends up being unformatted text." | ./post_matrix_msg.sh --gpg-pubkey my_gpg_pubkey.pub --encrypt "Encrypt this text." --unformatted --link https://matrix.org/

# How to generate your own matrix.org link with access token: https://matrix.org/docs/guides/getting-involved/#write-your-own-client

# TODO: For some reason formatted messages do not display properly on Element Android, but does so in Element Web.


f__show_useful_info () {
# This function is originally from initssh
# Shows IP addresses of interfaces so that it's easier to know to which IP address to connect.
echo "--- START INFO ---"
echo
echo "Hostname = $(hostname)"
echo
	if command -v ip >/dev/null
		then ip address | grep inet
	elif command -v ifconfig >/dev/null
		then ifconfig | grep inet
	else echo "ip command AND ifconfig not found!"
fi
echo
# Print sshd pubkeys so that we can check whether we're connecting to the right machine during hostkeycheck on the client.
for hostpubkey in /etc/ssh/ssh_host_*.pub; do ssh-keygen -lf "$hostpubkey"; done || echo "No pubkeys were found in /etc/ssh/ so sshd pubkeys could not be displayed!"
echo
echo "--- END INFO ---"
}


# [ -t 0 ] returns false when piping into the script, like: echo "hello" | ./script.sh
# Conversely, [ ! -t 0 ] returns true when something is piped into script.
# https://stackoverflow.com/a/2456870
if [ ! -t 0 ] ; then stdin="$(cat)" ; fi


# Actual logic for various flags.
while [ $# -gt 0 ] ; do
	case $1 in
		-g|--gpg-pubkey)
			if [ -f "$2" ]
				then gpg_pubkey="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else echo "ERR: Must specify the GPG pubkey file after -g or --gpg-pubkey flag!" ; exit 1
			fi
			;;
		-e|--encrypt|-u|--unformatted)
			if [ -n "$2" ] && [ -n "${2%%-*}" ]  # True if $2 is non-zero and doesn't start with a hyphen. Using substring processing in the 2nd test.
				then
					case $1 in
						-e|--encrypt)
							message="$(echo $2 | gpg --armor --encrypt --recipient-file "$gpg_pubkey" | perl -pe 's|\n|\\n|g')"
							;;
						-u|--unformatted)
							unformatted_text="$2"
							;;
					esac
					shift 2
			elif [ -n "$stdin" ]
				then
					case $1 in
						-e|--encrypt)
							message="$(echo $stdin | gpg --armor --encrypt --recipient-file "$gpg_pubkey" | perl -pe 's|\n|\\n|g')"
							;;
						-u|--unformatted)
							unformatted_text="$stdin"
							;;
					esac
					shift
			else echo "Please specify a message as argument or through STDIN" ; exit 1
			fi
			;;
		-i|--info)
			message="$(f__show_useful_info | gpg --armor --encrypt --recipient-file "$gpg_pubkey" | perl -pe 's|\n|\\n|g')"
			shift
			;;
		-l|--link)
			[ -n "$2" ] && url="$2" || { echo "ERR: URL after -l or --link must be provided!" ; exit 1 ; }
			shift
			;;
#		-x|--xstoken)  # Not really necessary since you're specifying te entire thing in the URL using -l or --link ?
#			echo "Entering -x case."
#			[ -n "$2" ] && xstoken="$2" || { echo "Please specify access token!" ; exit 1 ; }
#			shift
#			;;
		*)
			#if [ ! -t 0 ] ; then
			#	message="$(echo $stdin | perl -pe 's|\n|\\n|g')" # Take any input from STDIN.
			#fi
			break ;;
	esac
done


formatted_text="<pre><code>$message</code></pre>" # This formats into CommonMark's ``` code blocks.

if [ -n "$message" ] || [ -n "$unformatted_text" ] ; then
# See for this escaping madness: https://unix.stackexchange.com/a/577713
#curl -XPOST -d '{"msgtype":"m.text", "body":"'"$message"'", "format": "org.matrix.custom.html", "formatted_body": "'"$format_body"'"}' "$url"
#curl -XPOST -d '{"msgtype":"m.text", "body":"'"$message"'", "format": "org.matrix.custom.html", "formatted_body": "'"$unformatted_text"''"$formatted_text"'"}' "$url"
#curl -XPOST -d '{"msgtype":"m.text", "body":"```\n '"$message"' \n```", "format": "org.matrix.custom.html", "formatted_body": "'"$unformatted_text"''"$formatted_text"'"}' "$url"
curl -XPOST -d '{"msgtype":"m.text", "body":"```\n'"$message"'```", "format": "org.matrix.custom.html", "formatted_body": "'"$unformatted_text"''"$formatted_text"'"}' "$url"
else echo "Please specify text you want to have encrypted and/or unformatted text!" ; exit 1
fi


# Use this if you don't want code formatting:
#curl -XPOST -d '{"msgtype":"m.text", "body":"'"$message"'"}' "$url"
