#!/usr/bin/env sh

# Print battery information.

f__rm0 () ( sed 's|.\{3\}$||' ; )
# Alternative in AWK, but then it won't print 0 mA in the "Current now"
#f__rm0 () ( awk '{print substr($0, 1, length($0)-3)}' ; )

for bat in /sys/class/power_supply/BAT[0-999999999]*
do
	echo "$bat":
	manufacturer="$(cat "$bat"/manufacturer)"                                                                                ; echo "Manufacturer: $manufacturer"
	model="$(cat "$bat"/model_name)"                                                                                         ; echo "Model: $model"
	technology="$(cat "$bat"/technology)"                                                                                    ; echo "Technology: $technology"
	
	# Sometimes "charge" is called "energy" and "current" called "power"
	charge_f="$(cat "$bat"/charge_full 2>/dev/null | f__rm0) $(cat "$bat"/energy_full 2>/dev/null | f__rm0)"                 ; echo "Charge full: $charge_f mAh"
	charge_f_d="$(cat "$bat"/charge_full_design 2>/dev/null | f__rm0) $(cat "$bat"/energy_full_design 2>/dev/null | f__rm0)" ; echo "Charge full (design): $charge_f_d mAh"
	charge_now="$(cat "$bat"/charge_now 2>/dev/null | f__rm0) $(cat "$bat"/energy_now 2>/dev/null | f__rm0)"                 ; echo "Charge now: $charge_now mAh"
	current_now="$(cat "$bat"/current_now 2>/dev/null | f__rm0) $(cat "$bat"/power_now 2>/dev/null | f__rm0)"                ; echo "Current now: $current_now mA"
	
	capacity="$(cat "$bat"/capacity)"                                                                                        ; echo "Capacity: $capacity%"
	# p=part w=whole
	health="$(awk -v p=$(echo $charge_f) -v w=$(echo $charge_f_d) \
		'BEGIN {print (p / w) * 100}')"                                                                                        ; echo "Health: $health%"
	stop_threshold="$(cat "$bat"/charge_stop_threshold)"                                                                     ; echo "Charge stop threshold: $stop_threshold%"
	status="$(cat "$bat"/status)"                                                                                            ; echo "Status: $status"
	echo
done
