#!/bin/sh

# Timestamps and logs termperature to ~/logs/thermal/
# Best is to run this script like `watch -n5 -d 'sh log_thermal.sh'`

logdir=~/logs/thermal/

if command -v sensors > /dev/null
then
	if [ ! -d $logdir ]
	  then mkdir -p $logdir
	fi
	(echo "$(date +%F__%T%z__%a) " | tr -d '\n' ; sensors | awk '/Package/ {print $4}' ) | tee --append $logdir/thermal_"$(date +%F)".log
else
	echo "lm-sensors not found!" ; exit 1
fi
