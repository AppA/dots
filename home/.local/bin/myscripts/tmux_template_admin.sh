#!/usr/bin/env sh

# Admin panel
# Usage: tmux_template_admin.sh [-n|--session-name <name>] [-a|--tmux-args <tmux arguments>] 


# I do this because otherwise I have to specify targets to a lot of tmux commands. Yes I'm lazy :)
if [ -n "$TMUX" ] ; then echo "Please run this script outside of tmux. Thanks." ; return 1 ; fi

# You can specify custom tmux commands here, like a custom (testing) socket for a new server with -S
while [ $# -gt 0 ] ; do
	case $1 in
		-a|--tmux-args)
			if [ -n "$2" ]
				then args="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf  "ERR: Must specify custom tmux arguments if you want to use this flag!" ; exit 1
			fi ;;
		-n|--session-name)
			if [ -n "$2" ]
				then sesh="$2"
				shift 2
				else printf  "ERR: Must specify session name if you want to use this flag!" ; exit 1
			fi ;;
		*) break ;;
	esac
done

sesh="${sesh:=admin}"  # Default name for the session.

# Some config
if tmux $args new-session -d -s "$sesh" ; then true ; else exit 1 ; fi  # Create session, or else exit early.
tmux $args set-option pane-base-index 1  # Make pane index start at 1 instead of 0.



### W1: upd
tmux $args rename-window upd
tmux $args send-keys -t "$sesh":upd " upd"

### W2: per
tmux $args new-window -n per
#tmux $args select-window -t "$sesh":per
tmux $args send-keys " bc" Enter  # p1 (calc)
tmux $args split-window -v  # p2 (cal)
tmux $args split-window -h -t "$sesh":per.1  # p3 (random cmd)
# Display a calendar in the pane and refresh it every day after 00:00
tmux $args send-keys -t "$sesh":per.2 'clear && cal -A 3 ; while :; do date_1="$(date +%F)" ; sleep 2 ; date_2="$(date +%F)" ; if [ "$date_1" != "$date_2" ] ; then clear && cal -A 3 ; fi ; done' Enter
tmux $args select-pane -t "$sesh":per.3

### W3: mnt
tmux $args new-window -n mnt
tmux $args send-keys " watch -n 1 -d lsblk" Enter
tmux $args split-window -v -t "$sesh":mnt

### W4: dotfiles
tmux $args new-window -n dot
tmux $args send-keys -t "$sesh":dot " homeshick cd dots" Enter

### W5: file manager(s)
tmux $args new-window -n fm
tmux $args send-keys -t "$sesh":fm " vf"

### W6: gib
tmux $args new-window -n gib

### W6: comment-buffer
tmux $args new-window -n cb

### Finish it!
tmux $args select-window -t "$sesh":per  # Switch back to per
