#!/usr/bin/env sh

# GPG password decrypt wrapper for passphrase-only gpg files.

[ -z $1 ] && echo "Usage: $0 <filename.gpg>" && exit 1
if [ "${1##*.}" != gpg ] && [ "${1##*.}" != asc ] ; then echo "Filename must end in either .gpg or .asc in order to continue." ; exit 1 ; fi

echo "Passphrase please:"
read pass

enc_file="$1"
if [ "${1##*.}" = gpg ] ; then dec_file="${enc_file%*.gpg}"
elif [ "${1##*.}" = asc ] ; then dec_file="${enc_file%*.asc}"
fi

gpg --batch --passphrase "$pass" -o "$dec_file" --decrypt "$enc_file"
