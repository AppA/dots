#!/usr/bin/env sh

# TODO: Sending key sequences if already connected messes up things. Fix this.

tmux_sesh_n_pane="logstats:stats"

if [ $1 = 0 ]
then
	tmux send-keys -t "$tmux_sesh_n_pane".2 C-c
	tmux send-keys -t "$tmux_sesh_n_pane".4 C-c
	tmux send-keys -t "$tmux_sesh_n_pane".5 C-c
fi

if [ $1 = 1 ]
then
	tmux send-keys -t "$tmux_sesh_n_pane".2 "prettyping.bash example.org" Enter
	tmux send-keys -t "$tmux_sesh_n_pane".4 "pping_wlan.sh" Enter
	tmux send-keys -t "$tmux_sesh_n_pane".5 'tping.sh 15 "$(ip route | grep default | cut -d " " -f 3)"' Enter
fi
