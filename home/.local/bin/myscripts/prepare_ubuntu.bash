#!/bin/bash

# This script should best be run interactively. Just in case.
# Also this might not work on distros older than 18.04
# Also: Yeah it's a Bash script. Primarily because fzf requires it. Which Ubuntu doesn't ship with Bash these days anyways?

# Bail out if we're not using sudo:
if [ -z $SUDO_USER ]; then echo "This script requires to be run with sudo! Root privs alone is not sufficient!" >&2 && exit 1; fi

# Add user to the necessary groups
usermod --append --groups audio,libvirt,wireshark "$SUDO_USER"

# Bootstrap required tools
apt update && apt --yes install software-properties-common gnupg curl git

# Populate ~/sources
src="/home/$SUDO_USER/sources/"
if [ -d $src ] ; then cd $src ; else mkdir -p $src && cd $src ; fi
for url in \
	https://github.com/amanusk/s-tui \
	https://github.com/junegunn/fzf \
	https://github.com/macvk/dnsleaktest \
	https://github.com/soimort/translate-shell \
	; do git clone --depth 1 $url ; done
chown -R $SUDO_USER:$SUDO_USER $src

# Install fzf via install script
bash "$src"/fzf/install --bin

# Install Ansible via PPA, or else via pip3
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu
# PPA might not be available on certain Ubuntu versions, like it was for 20.04 - https://github.com/ansible/ansible/issues/68645
# pip3: Installing for user, so not root. This requires temporarily dropping privs.
#add-apt-repository --yes ppa:ansible/ansible || (apt install --yes python3-pip && sudo -u $SUDO_USER pip3 install --user --upgrade ansible)
# Fuck it, just install it with pip:
apt install --yes python3-pip && sudo -u $SUDO_USER pip3 install --user --upgrade ansible

## Install weechat PPA
## https://launchpad.net/~nesthib/+archive/ubuntu/weechat
#apt-key adv --keyserver keys.gnupg.net --recv-keys 11E9DE8848F2B65222AA75B8D1820DB22A11534E
#add-apt-repository --yes "deb https://weechat.org/ubuntu $(lsb_release -cs) main"

# Install Syncthing PPA
# https://apt.syncthing.net/
curl -s https://syncthing.net/release-key.txt | apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | tee /etc/apt/sources.list.d/syncthing.list

# Install relevant packages.
apt update ; apt --yes install \
	ansible \
	cpufrequtils \
	curl \
	gpg \
	htop \
	iotop \
	jq \
	lnav \
	macchanger \
	mtr-tiny \
	ncdu \
	net-tools \
	nethogs \
	nload \
	openssh-server \
	pm-utils \
	sshfs \
	sshuttle \
	syncthing \
	tmux \
	tree \
	vifm \
	vim \
	wavemon \
	weechat \
	weechat-perl \
	weechat-python \
	wget


# Optionally install graphical packages.
if [ "$1" = "-g" ] ; then

# Install Alacritty PPA
# https://github.com/alacritty/alacritty
add-apt-repository --yes ppa:mmstick76/alacritty

# Install mpv PPA
# https://launchpad.net/~mc3man/+archive/ubuntu/mpv-tests
add-apt-repository --yes ppa:mc3man/mpv-tests

# Prepare for i3 + rofi
# See https://i3wm.org/docs/repositories.html#_stable_releases
/usr/lib/apt/apt-helper download-file https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2021.02.02_all.deb keyring.deb \
SHA256:cccfb1dd7d6b1b6a137bb96ea5b5eef18a0a4a6df1d6c0c37832025d2edaa710 &&
dpkg -i sur5r-keyring_2021.02.02_all.deb &&
echo "deb https://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" >> /etc/apt/sources.list.d/sur5r-i3.list

# Populate ~/sources (now with GUI apps)
src="/home/$SUDO_USER/sources/"
if [ -d $src ] ; then cd $src ; else mkdir -p $src && cd $src ; fi
for url in \
	https://git.tartarus.org/simon/xcopy.git \
	https://github.com/GeorgeFilipkin/pulsemixer \
	https://github.com/amanusk/s-tui \
	https://github.com/gbraad/gauth \
	https://github.com/graupe/brownout \
	https://github.com/klaxalk/i3-layout-manager \
	; do git clone --depth 1 $url ; done
chown -R $SUDO_USER:$SUDO_USER $src

apt --yes install \
	alacritty \
	arandr \
	i3 \
	kde-spectacle \
	mpv \
	rofi \
	scrot \
	sxiv \
	xclip \
	xdotool

fi
