#!/usr/bin/env sh

# Script for checking 8-bit (256 color) support of the terminal.
# OG was terminalcolors.py (from here I think: https://gist.github.com/ohmystack/d0c0d62daafbfee6944de57d3d735bee)
# Rewritten for full POSIX sh compatibility by ChatGPT and me :)


f__out() {
command printf "tput setab %d; echo -n \"% 4d\"; tput setab 0\n" "$1" "$1" | sh
}


# normal colors 1 - 16
tput setaf 16

n=0
while [ $n -le 7 ]; do
	f__out "$n"
	n=$((n+1))
done
printf "\n" 

n=8
while [ $n -le 15 ]; do
	f__out "$n"
	n=$((n+1))
done
printf "\n\n"


# pretty colors 16 - 231
y=16
while [ "$y" -le 230 ] ; do
	z=0
	while [ "$z" -le 5 ] ; do
		f__out "$y"
		y=$((y+1))
		z=$((z+1))
	done
		printf "\n"
done
printf "\n"


# greyscale colors 232 - 255
n=232
while [ $n -le 255 ]; do
	f__out "$n"
	if [ "$n" -eq 237 ] || [ "$n" -eq 243 ] || [ "$n" -eq 249 ]
		then printf "\n"
	fi
	n=$((n+1))
done
printf "\n"

tput setaf 7
tput setab 0
