#!/bin/sh

# This script runs ~/.local/bin/myscripts/prettyping.sh to ping the Access Point of the WiFi network you're connected to.
# Though mainly it just pings the default gateway.

# tr --squeeze-repeats because of http://stackoverflow.com/a/4483833
#bash ~/.local/bin/myscripts/prettyping.sh -s 16 $(route -n | grep wlan | grep UGH | tr --squeeze-repeats ' ' | cut --delimiter=' ' --fields 2)

if command -v ip ; then prettyping.bash -s 16 $(ip route | awk '/default/ { print $3 }')
elif command -v route ; then prettyping.bash -s 16 $(route -n | awk '/UGH/ { print $2 }')
fi

# The packetsize (-s) argument of (pretty)ping has some limitations on some systems, see - https://bugs.launchpad.net/ubuntu/+source/iputils/+bug/1004350
