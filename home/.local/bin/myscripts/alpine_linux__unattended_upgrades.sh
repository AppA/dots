#!/usr/bin/env sh

# Poor man's unattended upgrades for Alpine Linux.
# This script upgrades Alpine Linux and automatically reboots if either kernel, SSL or SSH components are updated.
# Make this script executable and place it in /etc/periodic/15min/

unset gpg_pubkey
unset mat_link
unset post_matrix_msg_location
unset username
unset who

# Adjust these vars to your liking.
username="user"
who="machine_name"
post_matrix_msg_location="/home/$username/post_matrix_msg.sh"
gpg_pubkey="/home/$username/mygpg.pub"
mat_link='https://matrix.org/docs/guides/getting-involved/#write-your-own-client'


f__post_notif () {
/bin/sh $post_matrix_msg_location \
	--unformatted "$who UU:R" \
	--link "$mat_link"
}

f__post_error () {
if [ "$gpg_pubkey" ] && command -v gpg >/dev/null
then
	/bin/sh $post_matrix_msg_location \
		--gpg-pubkey "$gpg_pubkey" \
		--unformatted "$who UU:E" \
		--encrypt "$update_output" \
		--link "$mat_link"
else
	/bin/sh $post_matrix_msg_location \
		--unformatted "$who UU:E (GPG ERR)" \
		--link "$mat_link"
fi
}



# Notify if there's an error with apk upgrade process.
if ! update_output="$(apk --update-cache upgrade)"
then f__post_error ; exit 1
fi

# Log and notify if there are relevant (security) updates. Then reboot.
if [ "$(echo $update_output | grep -i 'linux-\|ssl\|ssh')" ]
then
	printf "TimeStamp: $(date +%F__%T%z__%a) \n$update_output \n" >> /var/log/apk_unattended.log
	f__post_notif
	reboot
else exit 0
fi
