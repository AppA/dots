#!/usr/bin/env sh

# A script for encoding, decoding, and extracting URLs.
# Source for encoding/decoding: https://gist.github.com/cdown/1163649
# https://en.wikipedia.org/wiki/Percent-encoding

f__die () {
cat << EOF
Usage:
$0 -e|--encode <url> | -d|--decode <url> | -x|--extract <file>
cat <file> | $0 -x|--extract
curl <url> | $0 -x|--extract

-e | --encode <url> : Encode URL into percent-encoding
-d | --decode <url> : Decode URL into percent-encoding
-x | --extract : Extract URL (from file or STDIN)
EOF
exit 1
}

if [ -z "$1" ] ; then f__die ; fi

# Extract URLs from text (files (like webpages))
# Focuses on portability rather than operation, so this might not catch all URLs.
# For a more complete solution, perhaps this might do the trick: https://daringfireball.net/2010/07/improved_regex_for_matching_urls
f__extract_urls () {
awk '
BEGIN { FS = "[ \t\n\"'"'"'()<>]" }
{
	for (i = 1; i <= NF; i++)
	{
		if ($i ~ /^https?:\/\/[^[:space:]]+$/)
		{
			gsub(/[.,;:]+$/, "", $i)
			print $i
		}
	}
}'
}

f__urlencode() {
string="$1"
for i in $(seq 1 ${#string}) ; do
	char=$(printf "%s" "$string" | cut -c $i)
	case "$char" in
		[a-zA-Z0-9.~_-] | '!' | '#' | '$' | '&' | "'" | '(' | ')' | '*' | '+' | ',' | '/' | ':' | ';' | '=' | '?' | '@' | '[' | ']' )
			printf "%s" "$char"
			;;
		*)
			printf '%%%02X' "'$char"
			;;
	esac
done
}

f__urldecode() {
echo "$1" | sed 's|+| |g ; s|%\([0-9a-fA-F][0-9a-fA-F]\)|\\\x\1|g' | xargs -0 printf '%b'
}


while [ $# -gt 0 ] ; do
	case $1 in
		-e|--encode)
			if [ -n "$2" ]
				then
					f__urlencode "$2"
					shift 2
				else f__die
			fi ;;
		-d|--decode)
			if [ -n "$2" ]
				then
					f__urldecode "$2"
					shift 2
				else f__die
			fi ;;
		-x|--extract)
			if [ -n "$2" ] && [ -f "$2" ]
				then
					f__extract_urls < "$2"
					shift 2
				elif [ ! -t 0 ]  # https://stackoverflow.com/a/2456870
					then f__extract_urls
					shift
				else f__die
				fi ;;
		*) break ;;
	esac
done
