#!/usr/bin/env sh

# Manually download Alpine Linux packages.
# Just point it to a package URL like https://pkgs.alpinelinux.org/package/v3.20/main/armv7/tmux
# And it will download to the current working dir, unless you specify a different directory as the following argument.

if [ -z $1 ] ; then echo "Usage: $0 <URL> [destination_dir]" ; exit 1 ; fi
if [ -z $2 ] ; then pkg_dl_dir=. ; else pkg_dl_dir="$2" ; fi

pkg_url="$1"
pkg_version="$(curl $pkg_url | grep -A 3 '<th class="header">Version</th>' | head -n 3 | tail -n 1 | sed 's| ||g')" # WARN: This grep flag might not work on Solaris.
pkg_dl_link="$(echo $pkg_url | sed 's|pkgs.|dl-cdn.|' | sed 's|package|alpine|')-${pkg_version}.apk"

curl --remote-name "$pkg_dl_link" --output-dir "$pkg_dl_dir"
