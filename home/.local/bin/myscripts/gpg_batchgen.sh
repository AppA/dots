#!/usr/bin/env sh

# Wrapper for non-interactive GPG keypair creation.
# If $GNUPGHOME isn't set, then current directory is used as $GNUPGHOME

f__err () {
printf "$1
Usage: $0 -n|--name <UID_name> [-c|--comment <comment>] [-k|--keyring <path/to/keyring>] -p|--passphrase <passphrase>"
exit 1
}

while [ $# -gt 0 ] ; do
	case $1 in
		-n|--name)
			if [ -n "$2" ]
				then name="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else f__err "ERR: Please specify name!"
			fi ;;
		-c|--comment)
			if [ -n "$2" ]
				then comment="$2"
				shift 2
				else f__err "ERR: Please specify a comment!"
			fi ;;
		-k|--keyring)
			if [ -n "$2" ]
				then keyring="$2"
				shift 2
				else f__err "ERR: Please specify a keyring!"
			fi ;;
		-p|--passphrase)
			if [ -n "$2" ]
				then pp="$2"
				shift 2
				else f__err "ERR: Please specify a passphrase!"
			fi ;;
		*) break ;;
	esac
done

if [ -z "$name" ] ; then f__err "ERR: Please specify name!" ; fi
if [ -z "$pp" ] ; then f__err "ERR: Please specify passphrase!" ; fi
if [ -z "$GNUPGHOME" ] ; then printf "\$GNUPGHOME is not set, so setting it to $PWD \n" ; export GNUPGHOME=. ; fi
if [ "$keyring" ]
	then
		krflag="--keyring $keyring"
		if [ ! -f "$keyring" ] ; then touch "$keyring" && chmod 600 "$keyring" ; fi
fi


# https://www.gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html
gpg --batch --no-default-keyring $krflag --gen-key << EOF
Key-Type: 1
Key-Length: 4096
Subkey-Type: 1
Subkey-Length: 4096
Name-Real: $name
Name-Comment: $comment
Expire-Date: 0
Passphrase: $pp
%commit
EOF
