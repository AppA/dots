#!/usr/bin/env sh

## Print all iptables rules
# OG Source: https://unix.stackexchange.com/a/372797
# Packet flow: https://upload.wikimedia.org/wikipedia/commons/3/37/Netfilter-packet-flow.svg
# Simpler version: https://0x0.st/inty.jpg

f__print_usage () {
# ${0##*/} expands to just the script name (without full path)
cat << EOF
Usage: ${0##*/} [flags]
EOF
}

# Check for missing dependencies.
for dep in \
	iptables-save \
	;
do
	command -v "$dep" >/dev/null || { echo "ERR: $dep not found. Please install $dep." ; f__print_usage ; exit 1 ; }
done


flags="$1"
flags="${flags:=-v -L --line-numbers}"
sep="============"

for table in $($privprefix iptables-save | grep "^\*" | sed 's|^*||g') ; do 
	printf '%s \n' "$sep $table table: $sep"
	$privprefix iptables -t "$table" $flags
	printf '\n \n'
done
