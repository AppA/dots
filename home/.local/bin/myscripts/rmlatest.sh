#!/usr/bin/env sh

# Removes the latest modified (dot)file or directory in the current or specified directory.
# Flags:
#   -d: Include directories
#   -h: Include hidden files (dotfiles)

# Initialize flags
include_dirs=0
include_dotfiles=0

# Parse command line options
while [ $# -gt 0 ] ; do
	case "$1" in
		-d) include_dirs=1 ; shift ;;
		-h) include_dotfiles=1 ; shift ;;
		-dh|-hd) include_dirs=1 ; include_dotfiles=1 ; shift ;;
		*) echo "Invalid option: $1" >&2
			 echo "Usage: $0 [-d|-h]" >&2
			 echo "Where -d is for directories and -h is for dotfiles." >&2
			 exit 1 ;;
	esac
done

# Initialize the variable to store the latest file
latest=

# Process regular files and directories if requested
for file in "${1:-.}"/* ; do
	# Check if the file exists. This is necessary because the wildcard may not expand
	# if there are no matching files, resulting in the literal string "${1:-.}/*"
	[ -e "$file" ] || continue
	
	# Process the file if it's not a directory, or if directories are included
	if [ "$include_dirs" = 1 ] || [ ! -d "$file" ]
		then # Update 'latest' if it's empty or if the current file is newer
			if [ -z "$latest" ] || [ "$file" -nt "$latest" ]
				then latest="$file"
			fi
	fi
done

# Process dotfiles if requested
if [ "$include_dotfiles" = 1 ]
	then
    for file in "${1:-.}"/.* ; do
			# Skip the current (.) and parent (..) directory entries
			case "$file" in
					"${1:-.}/." | "${1:-./..}") continue ;;
			esac
			[ -e "$file" ] || continue # Check if the file exists (same reason as above)

			# Process the file if it's not a directory, or if directories are included
			if [ "$include_dirs" = 1 ] || [ ! -d "$file" ]
				then # Update 'latest' if it's empty or if the current file is newer
					if [ -z "$latest" ] || [ "$file" -nt "$latest" ]
						then latest="$file"
					fi
			fi
		done
fi

# If a latest file was found, prompt for removal
if [ -n "$latest" ]
	then
		printf 'Remove %s ??? [yY/nN] ' "$latest"
		read yn
		case "$yn" in
			[yY]) rm -rvf "$latest" ;;  # Remove the file if user confirms
				*) exit 0 ;;  # Exit if user doesn't confirm
		esac
	else printf 'No files found.' ; exit 1
fi
