#!/usr/bin/env sh

# Rudimentary daemon script for low battery warnings.
# Requires: lsbat.sh
# Example: thr=93 ; batt_monitord.sh --threshold $thr --command "curl -d \"WARN: LOW BATT ($thr%)\" example.com ; backlight_blinkeblonker.sh"

help="Usage: $0 --threshold <value> --command <cmd>"
if [ -z "$1" ] ; then printf "$help" ; exit 1 ; fi

# Argument logic
while [ $# -gt 0 ] ; do
	case $1 in
		-t|--threshold)
			if [ "$2" -eq "$2" ]  # Test if number.
				then threshold="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf "ERR: Must specify the threshold amount!" ; exit 1
			fi ;;
		-c|--command)
			if [ -n "$2" ]
				then cmd="$2"
				shift 2
				else printf "ERR: Please specify a command!" ; exit 1
			fi ;;
		*) printf "$help" ; exit 1
			break ;;
	esac
done


# Main logic
action_taken=0  # Flag to ensure the command is ran only once
while true ; do
	# Extract the battery level percentage
	#batt_lvl="$(lsbat.sh | awk '/Capacity/ {gsub(/%$/, "", $2); print $2}')"
	# If multiple batteries exist, get the value of the one with the lowest amount.
	# TODO: Maybe add option to explicitly state which specific battery to monitor?
	batt_lvl="$(lsbat.sh | awk '/Capacity/ {gsub(/%$/, "", $2); print $2}' | sort -n | head -n1)"
	# The next line is mostly for debugging purposes:
	#echo "Timestamp: $(date +%F__%T%z__%a) - LVL: $batt_lvl"

	# Check if the battery level is below the threshold and the action has not been taken
	if [ "$batt_lvl" -le "$threshold" ] && [ "$action_taken" -eq 0 ]
		then
			eval "$cmd"  # Execute the command. WARN: Be careful!
			# Set the flag to indicate the action has been taken
			action_taken=1
		elif [ "$batt_lvl" -gt "$threshold" ] && [ "$action_taken" -eq 1 ]
			then action_taken=0  # Reset the flag if the battery level is above the threshold
	fi
	sleep 5
done
