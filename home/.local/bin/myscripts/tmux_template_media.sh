#!/usr/bin/env sh

# Session focused on media
# Usage: tmux_template_media.sh [-n|--session-name <name>] [-a|--tmux-args <tmux arguments>] 


# I do this because otherwise I have to specify targets to a lot of tmux commands. Yes I'm lazy :)
if [ -n "$TMUX" ] ; then echo "Please run this script outside of tmux. Thanks." ; return 1 ; fi

# You can specify custom tmux commands here, like a custom (testing) socket for a new server with -S
while [ $# -gt 0 ] ; do
	case $1 in
		-a|--tmux-args)
			if [ -n "$2" ]
				then args="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf  "ERR: Must specify custom tmux arguments if you want to use this flag!" ; exit 1
			fi ;;
		-n|--session-name)
			if [ -n "$2" ]
				then sesh="$2"
				shift 2
				else printf  "ERR: Must specify session name if you want to use this flag!" ; exit 1
			fi ;;
		*) break ;;
	esac
done

sesh="${sesh:=media}"  # Default name for the session.


# Some config
if tmux $args new-session -d -s "$sesh" ; then true ; else exit 1 ; fi  # Create session, or else exit early.
tmux $args set-option pane-base-index 1  # Make pane index start at 1 instead of 0.



### W1: VLC
tmux $args rename-window VLC
tmux $args send-keys -t "$sesh":VLC " firejail --seccomp vlc -vvv"

### W2: mpv
tmux $args new-window -d -n mpv
tmux $args send-keys -t "$sesh":mpv " mp"

### W3: bws
tmux $args new-window -d -n bws
tmux $args send-keys -t "$sesh":bws " vim -p ~/2sync/text_vcs/notes/audio/bitwig.md" 
tmux $args split-window -v -t "$sesh":bws

### W4: aud
tmux $args new-window -d -n aud
tmux $args send-keys -t "$sesh":aud " qjackctl"

### W5: aww
tmux $args new-window -d -n aww
tmux $args send-keys -t "$sesh":aww.1 " cd ~/Downloads/tmp/aww/vidz/" Enter
tmux $args send-keys -t "$sesh":aww.1 " source ../dump_vids.sh" Enter
tmux $args split-window -t "$sesh":aww
tmux $args send-keys -t "$sesh":aww.2 " cd ~/Downloads/tmp/aww/pix/" Enter
tmux $args send-keys -t "$sesh":aww.2 " source ../dump_pix.sh" Enter

### W6: mlinx
tmux $args new-window -d -n mlinx
tmux $args send-keys -t "$sesh":mlinx " sh ~/.local/bin/myscripts/guistuff/medialink_parsing/pass_medialink_to_mpv.sh" Enter



###
tmux $args select-window -t "$sesh":mpv  # Switch to mpv window.
