#!/usr/bin/env sh

# The tmux session that hosts "daemons"
# Usage: tmux_template_comms.sh [-n|--session-name <name>] [-a|--tmux-args <tmux arguments>] 

# I do this because otherwise I have to specify targets to a lot of tmux commands. Yes I'm lazy :)
if [ -n "$TMUX" ] ; then echo "Please run this script outside of tmux. Thanks." ; return 1 ; fi

# You can specify custom tmux commands here, like a custom (testing) socket for a new server with -S
while [ $# -gt 0 ] ; do
	case $1 in
		-a|--tmux-args)
			if [ -n "$2" ]
				then args="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf  "ERR: Must specify custom tmux arguments if you want to use this flag!" ; exit 1
			fi ;;
		-n|--session-name)
			if [ -n "$2" ]
				then sesh="$2"
				shift 2
				else printf  "ERR: Must specify session name if you want to use this flag!" ; exit 1
			fi ;;
		*) break ;;
	esac
done

sesh="${sesh:=daem}"  # Default name for the session.

# Some config
if tmux $args new-session -d -s "$sesh" ; then true ; else exit 1 ; fi  # Create session, or else exit early.
tmux $args set-option pane-base-index 1  # Make pane index start at 1 instead of 0.


### W1: network
tmux $args rename-window net
tmux $args split-window -v -t "$sesh":net

### W2: VPN
tmux $args new-window -n VPN
tmux $args send-keys -t "$sesh":VPN " # VPN" Enter
tmux $args split-window -v 

### W3: syncthing
tmux $args new-window -n st
tmux $args send-keys -t "$sesh":st " syncthing -no-browser"

### W4: torrents
tmux $args new-window -n torrents


###
tmux $args select-window -t "$sesh":VPN  # Switch back to VPN
