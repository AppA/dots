#!/usr/bin/env sh

# Extract archives, agnostic of format and tool.
# Originally sourced from here (I think): https://unix.stackexchange.com/a/7651 - but I extended it to be more robust and try magic numbers as well.
# For a more comprehensive tool, maybe try https://www.nongnu.org/atool/

if [ -f "$1" ]
	then
		case "$1" in
			*.xz)                   xz -d "$1"         ;;
			*.tar)                  tar -xvf "$1"      ;;
			*.gz|*.tgz|*.tar.gz)    tar -xvzf "$1"     ;;
			*.bz2|*.tbz2|*.tar.bz2) tar -xvjf "$1"     ;;
			*.rar|*.zip|*.7z)       7z x "$1"          ;;
			*.Z)                    uncompress -N "$1" ;;
			*) # Otherwise try with magic numbers: http://www.garykessler.net/library/file_sigs.html
				magic="$(dd if="$1" bs=1 count=4 2>/dev/null | xxd -p -c 4)"
				case "$magic" in
					fd37*)             xz -d "$1"         ;; # xz
					1f8b*)             tar -xvzf "$1"     ;; # tgz / tar.gz / gz
					425a*)             tar -xvjf "$1"     ;; # tbz2 / tar.bz2 / bz2
					7573*|2e2f*)       tar -xvf "$1"      ;; # tar (ustar) / tar (???)
					504b*|5261*|377a*) 7z x "$1"          ;; # zip / rar / 7z
					1f9d*)             uncompress -N "$1" ;; # Z
					*) echo "$1 can not be extracted via $0" ; exit 1 ;;
				esac
		esac
	else echo "$1 is not a valid file!" ; exit 1
fi
