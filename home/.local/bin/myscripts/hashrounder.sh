#!/bin/sh

# This script takes input from STDIN and then can run that input through multiple rounds of hashing.
# Usage: echo hashrounder.sh <hash_command> <amount_of_rounds>
# Example: echo "something" | hashrounder.sh sha256sum 42
if [ "$2" -eq 1 ]
	then "$1"
	else "$1" | $0 "$1" $(($2 - 1))
fi
