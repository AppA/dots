#!/usr/bin/env sh

# This script starts logging panes which I want to have logged.
# To stop logging, just issue `:pipe-pane` inside the relevant pane (or re-run this script to stop logging completely)

tmux_logdir="$HOME/logs/tmux"
if [ ! -d "$tmux_logdir" ] ; then mkdir -p "$tmux_logdir" ; fi

tmux pipe-pane -o -t admin:upd.1 "cat >> $tmux_logdir/tmux-pipe-pane--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"
tmux pipe-pane -o -t media:bws.2 "cat >> $tmux_logdir/tmux-pipe-pane--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"
tmux pipe-pane -o -t daem:VPN.1 "cat >> $tmux_logdir/tmux-pipe-pane--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"
tmux pipe-pane -o -t daem:torrents.1 "cat >> $tmux_logdir/tmux-pipe-pane--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"
tmux pipe-pane -o -t daem:st.1 "cat >> $tmux_logdir/tmux-pipe-pane--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"



# For some reason the following doesn't work as expected. See https://github.com/tmux-plugins/tmux-logging/issues/27#issuecomment-683483882
#ansi_codes="(\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]|^M)"
#tmux pipe-pane -o -t logstats:teest.1 "cat | sed -r 's/$ansi_codes//g' >> ~/logs/tmux/tmux-log--test--with_sed--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"
# This however, does work as expected and was used to test this script:
#tmux pipe-pane -o -t logstats:teest.1 "cat >> $tmux_logdir/tmux-log--test--no_sed--%Y-%m-%d-%H:%M:%S--#{session_name}-#{window_index}:#{window_name}-p#{pane_index}:#{pane_title}.log"



# And this was intended to be used in combination with https://github.com/tmux-plugins/tmux-logging
# Unfortunately that doesn't work for some reason.

## Log updates
#tmux send-prefix -t admin:upd.1
#tmux send-keys -t admin:upd.1 "P"
#
## Log VPN
#tmux send-prefix -t daem:VPN.1
#tmux send-keys -t daem:VPN.1 "P"
#
## Log synapse-bt daemon
#tmux send-prefix -t daem:torrents.1
#tmux send-keys -t daem:torrents.1 "P"
#
## Test shit
#tmux send-prefix -t logstats:teest.1
#tmux send-keys -t logstats:teest.1 "P"
