#!/bin/sh

# A wrapper around git tools to remove git submodules.

# This tests (within reason) whether we're inside a git repo.
# This means that .git/objects/ and .git/refs/ have to be present in order for the test to pass.
if [ ! -d .git/objects/ ] || [ ! -d .git/refs/ ] ; then echo "Please run this script at the root of a git repo!" ; exit 1 ; fi

case $1 in
	-s | --soft ) shift
				if [ "$1" = -a ] ; then git submodule deinit .
				else
					for submodpath in "$@" ; do git submodule deinit "$submodpath" ; done
					# Clears the submodule dir. Undo this using "git submodule init <submodule_path> && git submodule update <submodule_path>"
				fi
	;;
	-h | --hard ) shift
				# Still leaves some trace in .git/config so that in the future you can still re-add the submodule if you want.
				# NO UNDO POSSIBLE (other than manually re-adding the submodule!)
				if [ "$1" = -a ]; then 
					git submodule status | awk '{print $2}' | while read -r submodpath
					do git rm -rf "$submodpath" && rm -rf .git/modules/"$submodpath"
					done
				else 
					for submodpath in "$@"
					do git rm -rf "$submodpath" && rm -rf .git/modules/"$submodpath"
					done
				fi
	;;
	* ) 
		echo 'Usage: git-submodule-rm -s|-h [-a | submodule_path ...]'
		echo '-s (--soft) : Simply de-initialize the submodule(s). Undo by doing "git submodule init <submodule_path> && git submodule update <submodule_path>"'
		echo '-h (--hard) : Completely remove the submodule. Still leaves trace in .git/config though.'
		echo 'Either -s or -h take -a as argument, which soft/hard removes ALL the submodules. Dangerous!'
		exit 1
	;;
esac
