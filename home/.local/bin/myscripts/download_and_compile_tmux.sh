#!/bin/sh

# Downloads (by default latest) tmux release source and compiles it.
# Only tested this on Ubuntu, but this script is meant to support deb, rpm and OSX/MacOS systems in the future.
# TODO: Add option to patch grid.c with malloc_trim (see tmux_memleak_fix.txt)

# Requires root privileges

if [ "$(id -u)" != 0 ]; then echo "This script requires root privileges." >&2 && exit 1; fi

# TODO: Make this also work properly on yum/RH-based systems. Apparently checkinstall is not there. Also consult this (old!) doc: https://gist.github.com/ryin/3106801/

# TODO: Add prepare_system_mac
# To install homebrew: /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

f__prepare_system_debian () {
	apt-get -y remove tmux
	apt-get update
	# This method could be used to do a simple "apt-get build-dep tmux", but is perhaps too invasive? See: https://askubuntu.com/a/1011675
	#sed -i~orig -e 's/# deb-src/deb-src/' /etc/apt/sources.list
	#apt-get update && apt-get -y build-dep tmux
	apt-get -y install wget tar git automake libevent-dev libncurses-dev checkinstall
}

f__prepare_system_rpm () {
	yum -y remove tmux
	yum update
	yum -y install wget tar git automake libevent-devel ncurses-devel glibc-static checkinstall
}


f__compile_and_install () {
	if [ "$tmux_version" = git_master ]; then 
		cd tmux/ &&
		sh autogen.sh
		./configure && make &&
		checkinstall -y --install --type "$system" --pkgname tmux --pkgversion "$tmux_version" --pkglicense ISC
	else # If it's just the release version though...
		tar -xf tmux-"$tmux_version".tar.gz && cd tmux-"$tmux_version" && 
		./configure && make && 
		checkinstall -y --install --type "$system" --pkgname tmux --pkgversion "$tmux_version" --pkglicense ISC
	fi
}

if command -v apt-get; then echo "This is most probably a dpkg-based distro, continuing with Debian install" && system=debian && f__prepare_system_debian; fi
if command -v yum; then echo "This is most probably an rpm-based distro, continuing with Debian install" && system=rpm && f__prepare_system_rpm; fi

if [ -z "$1" ]; then
tmux_github_url="$(curl -s https://api.github.com/repos/tmux/tmux/releases/latest | grep 'browser_' | cut -d\" -f4 | grep '.tar.gz')"
tmux_version="$(echo "$tmux_github_url" | awk -F "/" '{print $8}')" && echo "The tmux version you're about to download is $tmux_version"
wget "$tmux_github_url" && f__compile_and_install

	# This regex should only match tmux version number. For example: 2.8
	# https://stackoverflow.com/a/19715367
	elif echo "$1" | grep -Pq '[0-9]+\.[0-9]+' ; then tmux_version="$1"
	wget https://github.com/tmux/tmux/releases/download/"$tmux_version"/tmux-"$tmux_version".tar.gz && f__compile_and_install
		elif [ "$1" = master ]; then git clone --depth 1 https://github.com/tmux/tmux/ && tmux_version=git_master && f__compile_and_install
fi
