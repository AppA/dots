#!/bin/sh

# This is to be used in vim for turning HN links into Markdown formatted bullet point links.
# Usually I have \' mapped for this. See ~/.vimrc

url="$1"
#curled_url="$(curl --silent $url)"
if [ -f "$url" ]
	then curled_url="$(cat $url)"
	else curled_url="$(curl --silent $url)"
fi

sitebit_comhead="$(echo "$curled_url" | grep -m1 'class="sitestr"' | sed 's/>/\n/g' | awk 'NR == 20' | sed 's/<.*//g')"

f__final_filter () {
sed 's|>|\n|g' | awk 'NR == 16' | sed \
	-e 's|<.*||g' \
	-e 's|&gt;|>|g' \
	-e 's|&lt;|<|g' \
	-e 's|&amp;|\&|g' \
	-e "s|&#x27;|'|g" \
	-e "s|&#x2F;|/|g" \
	-e 's|&quot;|"|g'
}

# Does not seem to work after ~2022-09-28
#title="$(echo "$curled_url" | grep 'class="titlelink"' | sed 's/>/\n/g' | awk 'NR == 15' | sed 's/<.*//g')"
#sitebit_comhead="$(echo "$curled_url" | grep 'class="sitestr' | sed 's/>/\n/g' | awk 'NR == 19' | sed 's/<.*//g')"

if [ -z "$sitebit_comhead" ]  # $sitebit_comhead would be missing if it's a post without link (like Ask HN / Tell HN)
	then 
		title="$(echo "$curled_url" | grep -m1 'href="item?' | f__final_filter)"
		printf %s "* [$title]($url)"
	else 
		title="$(echo "$curled_url" | grep -m1 'class="titleline"' | f__final_filter)"
		printf %s "* [$title ($sitebit_comhead)]($url)"
fi
