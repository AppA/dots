#!/bin/sh

#### vim_process_swap

# Source - http://vim.wikia.com/wiki/Swap_file_%22...%22already_exists!_-_so_diff_it?useskin=monobook
# But I modified it to be fully POSIX compliant!

# I sometimes get lots of swap files when my laptop loses power whlie vim is open. Using the above tips I created a bash function to help. It checks to see if the swap file is being used by an active vim process and if it isn't, it recovers the swap file into a recovery file (leaving the original file in place). It then compares the original file against recovered file; if they are identical it automatically removes the recovered file and swap file. If the recovered file and original file aren't the same, but the original file is newer than the swap file it asks if the user if they want to just delete the recovered file, or if they want to view a diff. If the swap file is newer than the original file and the recovered file is different than the original file, then vimdiff is opened so you can move any differences into the original file. When vimdiff is exited the function cleans up the old swap file and recovered file.

# Usage is simple: vim_process_swap file [swapfile [recoverfile]] swapfile and recoverfile are optional.
# If you are using the function in a loop over swap files, you can use the script like so: vim_process_swap -s $swapfile and you won't need to pass the real filename. Note that when using the -s flag that it isn't possible to specify a recoveryfile name or location.

# TODO: Try to do this without `Cannot stat file /proc/*/fd/*: Permission denied` appearing in the output.

vim_process_swap () {
    unset swapfile_first=0 ; swapfile_first=0
    while true; do
        case "$1" in
            ""|-h|--help)
                echo "usage: vim_process_swap file [swapfile [recoverfile]]" >&2
                return 1;;
            -s)
                shift
                swapfile_first=1;;
            *)
                break;;
        esac
    done
    unset realfile ; realfile=$(readlink -f "$1")
    unset path ; path=$(dirname "$realfile")
    unset realname ; realname=$(basename "$realfile")
    if [ $swapfile_first -eq 1 ]; then
        unset swapfile ; swapfile=$realfile
        #realname=${realname:1:-4}
				# https://github.com/koalaman/shellcheck/wiki/SC3057
				realname="$(realname=${realname#?} realname=${realname%????}; printf %s $realname)"
        realfile="${path}/${realname}"
    else
        unset swapfile ; swapfile=${2:-"${path}/.${realname#.}.swp"}
    fi
    unset recoverfile ; recoverfile=${3:-"${path}/${realname}-recovered"}
    unset lastresort ; lastresort=0
    for f in "$realfile" "$swapfile"; do
        if [ ! -f "$f" ]; then
            echo "File $f does not exist." >&2
            return 1
        elif fuser "$f"; then
            echo "File $f in use by process." >&2
            return 1
        fi
    done
    if [ -f "$recoverfile" ]; then
        echo "Recover file $recoverfile already exists. Delete existing recover file first." >&2
        return 1
    fi
    vim -u /dev/null --noplugin -r "$swapfile" -c ":wq $recoverfile"
    if cmp -s "$realfile" "$recoverfile"; then
        rm "$swapfile" "$recoverfile"
    # https://unix.stackexchange.com/questions/449741/performing-nt-ot-test-in-a-posix-sh
    elif [ -n "$(find $realfile -newer $swapfile)" ] && [ $(stat -c%s "$realfile") -gt $(stat -c%s "$recoverfile") ]; then
        echo "Swapfile is older than realfile, and recovered file is smaller than realfile"
        if _prompt_yn "Delete recovered file and swapfile without doing diff?"; then
            rm "$swapfile" "$recoverfile"
        else
            lastresort=1
        fi
    else
        lastresort=1
    fi
 
    if [ "$lastresort" -ne 0 ]; then
        rm "$swapfile"
        vim -d "$recoverfile" "$realfile"
        if _prompt_yn "Delete recovered file?"; then
            rm "$recoverfile"
        fi
    fi
}



_prompt_yn () {
    while true; do
        echo "$1 [y|n] "
        read yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

vim_process_swap "$1"
