#!/usr/bin/env sh

# Network monitoring wrapper.

f__die () {
cat << EOF
Usage: $0 [-p|--print <network_interface>] | [-a|-all] | [-r|--route] | [-l|--listen] | [-i|--interface-listen <network_interface>]
-p | --print <interface> : Only prints the first IP address of the specified network interface.
-a | --all: Display all TCP and UDP connections.
-r | --route: Display IPv4 routing table.
-l | --listen: See all listening services.
-i | --interface-listen <interface> : Only see listening services on the IP address of the specified network interface.
EOF
exit 1
}

[ $# -eq 0 ] && f__die

while [ $# -gt 0 ] ; do
	case $1 in
		-p|--print)
			if [ -n "$2" ] && [ "${2#-}" = "$2" ]  # This comparison checks if the value of $2 after removing a leading hyphen (if present) is the same as the original $2. If they're the same, it means $2 didn't start with a hyphen, and as such is (most likely) not another flag.
				then iface="$2" ; justprint=1
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else f__die
			fi ;;
		-a|--all)
			if [ $# -gt 1 ] ; then f__die ; fi  # No other flags / arguments should surround this flag.
			all=1
			shift
			;;
		-r|--route)
			if [ $# -gt 1 ] ; then f__die ; fi
			route=1
			shift
			;;
		-l|--listen)
			if [ $# -gt 1 ] ; then f__die ; fi
			listen=1
			shift
			;;
		-i|--interface-listen)
			if [ -n "$2" ] && [ "${2#-}" = "$2" ]
				then iface="$2" ; ilisten=1
				shift 2
				else f__die
			fi ;;
		*) f__die
	esac
done


# Get the IP address of the specified interface.
if iface_info="$($(command -v ip) addr show dev $iface)" >/dev/null 2>&1
	then iface_ip="$(echo "$iface_info" | awk '/inet / { split ($2, a, "/") ; print a[1] ; exit }')"
elif iface_info="$($(command -v ifconfig) $iface)" >/dev/null 2>&1
	then iface_ip="$(echo "$iface_info" | awk '/inet/ { print $2 ; exit }')"
	else exit 1
fi

if [ -z "$iface_ip" ] ; then echo "ERR: No IP address found for $iface" ; exit 1 ; fi
if [ -n "$justprint" ] ; then echo "$iface_ip" ; exit 0 ; fi  # When the -p or --print flag is specified.


# Test and set which network monitoring tools are going to be used.
if netstat -tulpn >/dev/null 2>&1 ; then netmoncmd="netstat"
elif command -v ss >/dev/null ; then netmoncmd="ss"
else netmoncmd="netstat_bsd"
fi

if [ -n "$listen" ]
	then # Listening services.
		if [ "$netmoncmd" = netstat ] ; then $privprefix watch -d "netstat -tulpn"
		elif [ "$netmoncmd" = ss ] ; then $privprefix watch -d "ss -tulpn"
		else while true ; do clear ; $privprefix netstat -an | grep LISTEN ; sleep 2 ; clear ; done
		fi
elif [ -n "$ilisten" ]
	then # Listening on specific network interface.
		if [ "$netmoncmd" = netstat ]  ; then $privprefix watch -d "netstat -tupwn | grep $iface_ip"
		elif [ "$netmoncmd" = ss ] ; then $privprefix watch -d "ss -tupwn | grep $iface_ip | column -t"
		else while true ; do clear ; $privprefix netstat -an | grep "$iface_ip" ; sleep 2 ; clear ; done
		fi
elif [ -n "$all" ]
	then # All connections.
		if [ "$netmoncmd" = netstat ]  ; then $privprefix watch -d "netstat -tupwn"
		elif [ "$netmoncmd" = ss ] ; then $privprefix watch -d "ss -tupwn | column"
		else while true ; do clear ; $privprefix netstat -an -f inet | grep -v LISTEN ; sleep 2 ; clear ; done
		fi
elif [ -n "$route" ]
	then # Routing table.
		if route -n >/dev/null 2>&1 ; then watch -n 2 -d "route -n"
		elif command -v ip >/dev/null ; then watch -n 2 -d "ip route"
		elif command -v netstat >/dev/null ; then while true ; do clear ; netstat -rn -f inet ; sleep 4 ; clear ; done # BSD IPv4
		fi
fi
