#!/usr/bin/env sh

# POSIX compliant script to replace UUIDs with identical new UUIDs.
# This means that recurring / duplicate UUIDs will be replaced with new recurring / duplicate UUIDs.

# Function to generate a new UUID, adjust according to your system.
# For example, uuidgen or openssl might be used on non-Linux systems instead of the command on the line below.
f__generate_uuid() { cat /proc/sys/kernel/random/uuid ; }

file_old="$1"
file_new="$2"  # This file will get the UUIDs replaced.
if [ "$file_old" = "$file_new" ] ; then echo "ERR: Filenames must differ!" ; exit 1 ; fi
cp "$file_old" "$file_new"

# Actual looptydoop:
strings "$file_old" | grep -oE '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[89ab][0-9a-f]{3}-[0-9a-f]{12}' | while IFS= read -r line ; do
uuid="$line"
	if echo "$seen_uuids" | grep -q "$uuid"  # Check if this UUID has already been seen
		then  # If yes, get the new UUID from seen UUIDs
			new_uuid=$(echo "$seen_uuids" | grep "$uuid" | cut -d ' ' -f 2)
		else  # If not, generate a new UUID and add it to seen UUIDs
			new_uuid=$(f__generate_uuid)
			seen_uuids="$seen_uuids$uuid $new_uuid\n"
	fi
line=$(echo "$line" | sed "s|$uuid|$new_uuid|g")  # Replace the old UUID with the new UUID in the line
search_and_replace_string_in_binfile.sh --no-backup --file "$file_new" --original "$uuid" --replacement "$new_uuid"  # Actual command to replace the UUID in the (bin)file.
done
