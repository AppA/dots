#!/bin/sh

# Install / update yt-dlp.

urlbase=https://github.com/yt-dlp/yt-dlp
latesthash="$(curl --silent --location $urlbase/releases/latest/download/SHA2-256SUMS | awk '$2 == "yt-dlp" { print $1 }' )"
#latestrel="$(curl --silent $urlbase/releases/latest/ | grep releases | awk -F'[""]' '{print $2}')"
latestrel="$(curl --silent --location --head --output /dev/null --write-out '%{url_effective}' $urlbase/releases/latest/)"
execdir=~/.local/bin

if [ "$(sha256sum "$execdir"/yt-dlp | awk '{ print $1 }')" = "$latesthash" ]
	then 
		echo "INFO: Local binary hash digest ($latesthash) ..."
		echo "...matches that of the latest stable release ($latestrel) \n -> No action required!"
	else
		echo "INFO: No (new) local binary found, attempting to download one..."
		cd $execdir || mkdir -p $execdir && cd $execdir
		cp -a -f yt-dlp yt-dlp.bak
		curl --location --remote-name "$urlbase/releases/latest/download/yt-dlp"
		if [ "$(sha256sum yt-dlp  | awk '{ print $1 }')" = "$latesthash" ] 
			then echo "INFO: Good news, hash digest of the downloaded binary matches the hash digest file on Github!"
				chmod a+rx yt-dlp && ln -f -s yt-dlp youtube-dl && echo "INFO: yt-dlp has been installed / updated!" ; echo "INFO: Latest release is: $latestrel"
			else echo "ERROR: Hash mismatch!" ; rm yt-dlp ; cp -a yt-dlp.bak yt-dlp ; exit 1
		fi
fi
