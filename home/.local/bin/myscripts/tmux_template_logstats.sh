#!/usr/bin/env sh

# Basic tmux system stats session
# Usage: tmux_template_logstats.sh [-n|--session-name <name>] [-a|--tmux-args <tmux arguments> [-i|--network-interface <network_interface]]

# TODO: Some network related stuff only happens when connection is initiated and disconnected. Currently a manual process.


# I do this because otherwise I have to specify targets to a lot of tmux commands. Yes I'm lazy :)
if [ -n "$TMUX" ] ; then echo "Please run this script outside of tmux. Thanks." ; return 1 ; fi

# You can specify custom tmux commands here, like a custom (testing) socket for a new server with -S
while [ $# -gt 0 ] ; do
	case $1 in
		-a|--tmux-args)
			if [ -n "$2" ]
				then args="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else printf "ERR: Must specify custom tmux arguments if you want to use this flag!" ; exit 1
			fi ;;
		-n|--session-name)
			if [ -n "$2" ]
				then sesh="$2"
				shift 2
				else printf "ERR: Must specify session name if you want to use this flag!" ; exit 1
			fi ;;
		-i|--network-interface)
			if [ -n "$2" ]
				then iface="$2"
				shift 2
				else printf "ERR: Must specify network interface if you want to use this flag!" ; exit 1
			fi ;;
		*) break ;;
	esac
done

sesh="${sesh:=logstats}"  # Default name for the session.

# Prompt for password without printing to terminal while being 100% POSIX: https://unix.stackexchange.com/a/222977
stty -echo
printf "sudo | doas pass plz: " # I need this for... stuff.
read -r REPLY
stty echo


# Some config
if tmux $args new-session -d -s "$sesh" ; then true ; else exit 1 ; fi  # Create session, or else exit early.
tmux $args set-option pane-base-index 1  # Make pane index start at 1 instead of 0.



### W1: stats
tmux $args rename-window stats
if command -v btop >/dev/null ; then tmux $args send-keys "btop" Enter
elif command -v htop >/dev/null ; then tmux $args send-keys "htop" Enter
else tmux $args send-keys "top" Enter
fi

# P2: prettyping
tmux $args split-window -v 
tmux $args send-keys " # prettyping" Enter

# P4: prettyping_local
tmux $args split-window -v 
tmux $args send-keys " # prettyping_local" Enter

# P3: mem & storage capacity
tmux $args split-window -h -t "$sesh":stats.2
if command -v watch >/dev/null
	then
		if df -h -x tmpfs >/dev/null 2>&1
			then tmux $args send-keys 'watch -d "df -h -x tmpfs"' Enter
			else tmux $args send-keys 'watch -d "df -h"' Enter
		fi
	else tmux $args send-keys 'while true ; do clear ; df -h ; sleep 4 ; clear ; done' Enter
fi

# P5: tping
tmux $args split-window -h -t "$sesh":stats.4
tmux $args send-keys " # tping.sh" Enter


### W2: logs
tmux $args new-window -n logs
if command -v lnav
	then
		tmux $args send-keys "lnav -r" Enter # P1
		tmux $args split-window -v
		tmux $args send-keys "lnav -r /var/log/kern.log" Enter # P2
	else
		if tail --help 2>&1 | grep -q -- '-F'
			then tailflag="-F"
			else tailflag="-f"
		fi
		tmux $args send-keys "tail $tailflag /var/log/messages" Enter
		tmux $args split-window -v
		tmux $args send-keys "tail $tailflag /var/log/kern.log" Enter
fi
tmux $args select-pane -t "$sesh":logs.1 
tmux $args resize-pane -Z  # Zoom P1 to entire window



### W3: network monitoring (cont.)
tmux $args new-window -n netmon 

# Test and set which network monitoring tools are going to be used.
if netstat -tulpn >/dev/null 2>&1 ; then netmoncmd="netstat"
elif command -v ss >/dev/null ; then netmoncmd="ss"
else netmoncmd="netstat_bsd"
fi


# P1: listening services
tmux $args send-keys "netmonwrapper.sh --listen" Enter

# P3: connections
tmux $args split-window -h 
tmux $args send-keys "netmonwrapper.sh --all" Enter

# P4: packets / connections from machine
tmux $args split-window -v
tmux $args send-keys "netmonwrapper.sh --interface-listen $iface" Enter

# P2: routing table
tmux $args select-pane -t "$sesh":netmon.1
tmux $args split-window -v
tmux $args send-keys "netmonwrapper.sh --route" Enter




### W4: WiFi signal monitoring
tmux $args new-window -n wifimon
tmux $args send-keys "wavemon" Enter  # P1
tmux $args split-window -v
tmux $args send-keys "wavemon" Enter  # P2
tmux $args send-keys -t "$sesh":wifimon.2 F2



### Finishing touches.
# Pass the passwords to relevant tmux panes.
sleep 2
tmux $args send-keys -t "$sesh":netmon.1 "$REPLY" Enter
tmux $args send-keys -t "$sesh":netmon.3 "$REPLY" Enter
tmux $args send-keys -t "$sesh":netmon.4 "$REPLY" Enter

tmux $args select-window -t "$sesh":logs # Have logs as the last window.
tmux $args select-window -t "$sesh":stats # Switch back to stats
tmux $args select-pane -t "$sesh":stats.3
#tmux $args attach-session -t "$sesh" # Finally, attach the session to the terminal!
