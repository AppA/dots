#!/usr/bin/env sh

f__print_help () {
cat << EOF

This script searches and replaces a string inside a binary (such as a .bwproject file)

Usage:
this_script.sh --file <binfile> --original <original_string> --replacement <final_string>
EOF
exit 1
}


[ $# -eq 0 ] && f__print_help  # Print help / usage info if no arguments are given.

while [ $# -gt 0 ] ; do
	case $1 in
		-f|--file)
			if [ -f "$2" ]
				then file="$2"
				shift 2  # Shift 2 arguments since another argument ($2) was specified for this particular flag. So 2 arguments have been processed and are no longer relevant.
				else echo "ERR: Must specify the filename!" ; f__print_help
			fi
			;;
		-o|--original)
			if [ -n "$2" ]
				then original_string="$2"
				shift 2
				else echo "ERR: Must specify the original string you intend to replace!" ; f__print_help
			fi
			;;
		-r|--replacement)
			if [ -n "$2" ]
				then replaced_string="$2"
				shift 2
				else echo "ERR: Must specify the replacement string you intend to replace the original string with!" ; f__print_help
			fi
			;;
		*)
			f__print_help
			break ;;
	esac
done


[ -z "$original_string" ] && { "ERR: Must specify the original string you intend to replace! (-o | --original)" ; f__print_help ; }

if [ "${#original_string}" -ne "${#replaced_string}" ]
	then echo "ERR: String length (amount of characters) must be the same for --original and --replacement !" ; f__print_help
fi

cp -a "$file" "$file"__backup

perl -0777 -i -pe "binmode \$_ for \*STDIN, \*STDOUT, \*ARGVOUT; s{$original_string}{$replaced_string}g" "$file"
