#!/usr/bin/env sh

# Rename the symlink and the target it points to / originated from.
# If symlink is in the same dir as the target / source, then the target / source gets renamed.

# Argument logic and preliminary tests.
if [ $# -eq 0 ]
	then printf "Usage: $0 <old_symlink_name> <new_symlink_name>" ; exit 1
	else 
		if [ ! -h "$1" ] ; then printf "ERR: $1 MUST be a symbolic link!" ; exit 1 ; fi
		if [ -f "$2" ] ; then printf "WARN: $2 already exists, overwrite? [Y/N] "
			read answer
			case "$answer" in
				[Yy]* ) printf "WARN: Overwriting the destination file...""\n" ;;
				[Nn]* ) exit 1 ;;
				* ) echo "Please answer Y/y or N/n." ;;
			esac
		fi
fi


# Define the current symlink and the new name for the target
symlink="$1"
new_symlink_and_target_name="$2"

# This is the most POSIX-compliant way I found to get the absolute path of the target. 
# Relies on readlink (not POSIX) and otherwise ls (POSIX, but breaks when spaces are present in the file)
if command -v readlink >/dev/null
	then target="$(readlink "$symlink")"
	else target="$(ls -l "$symlink" | awk '{print $NF}')" ; printf "WARN: Readlink not available, files with spaces will break""\n" ;
fi

# Construct the new path for the target
new_target_path="$(dirname "$target")/$new_symlink_and_target_name"
new_symlink_path="$(dirname "$symlink")/$new_symlink_and_target_name"

mv "$symlink" "$new_symlink_path"  # Rename the symlink first
mv "$target" "$new_target_path"  # Now rename the target

ln -s -f "$new_target_path" "$new_symlink_path"  # Create a new symlink pointing to the renamed target

printf "$symlink -> $new_symlink_path"
