#!/usr/bin/env sh

# Display timestamped pings to an address (example.com if no address is specified)

if [ -z "$1" ] && [ -z "$2" ] ; then echo "Usage: $0 <interval (in sec)> [address]" ; exit 1
elif [ -z "$2" ]
	then
		case "$1" in
			''|*[!0-9]*) ping www.example.com 2>&1 | while IFS= read -r line ; do printf '[%s] %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$line"; done ;;
			*) ping -i "$1" www.example.com 2>&1 | while IFS= read -r line ; do printf '[%s] %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$line"; done ;;
		esac
else
	case $1 in
		''|*[!0-9]*) ping "$2" 2>&1 | while IFS= read -r line ; do printf '[%s] %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$line" ; done ;;
		*) ping -i "$1" "$2" 2>&1 | while IFS= read -r line ; do printf '[%s] %s\n' "$(date '+%Y-%m-%d %H:%M:%S')" "$line" ; done ;;
	esac
fi
