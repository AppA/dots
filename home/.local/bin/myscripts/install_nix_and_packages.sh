#!/usr/bin/env sh

# This script installs Nix package manager and specified packages (or rather: declared) in ~/.config/nixpkgs/config.nix


help="Usage: script.sh -m|--multi-user -p|--pkg_group package_group ... | -a|--all"

[ $# -eq 0 ] && { echo "$help" ; exit 1 ; }

# Dep mgmt.
for dep in \
	awk \
	sed \
	wget \
	xz \
	;
do 
	command -v "$dep" >/dev/null || { echo "ERR: $dep not found. Please install $dep." ; exit 1 ; } 
done

if [ -f ~/.config/nixpkgs/config.nix ]
then available_package_groups="$(awk '/^[[:blank:]]*packages__/ { print "nixpkgs."$1 }' ~/.config/nixpkgs/config.nix)"
else echo "ERR: ~/.config/nixpkgs/config.nix not found!" ; exit 1
fi

if command -v sudo >/dev/null
	then privprefix=sudo
elif command -v doas >/dev/null 
	then privprefix=doas
	else echo "ERR: doas and sudo not found!" ; exit 1
fi

# Flag / argument logic.
while [ $# -gt 0 ] ; do
	case $1 in
		-m|--multi-user)
			mode=2
			shift 1
			;;
		-p|--pkg_group)
			shift 1
			case $1 in
				-a|--all) pkg_group=$available_package_groups ;;
				*) 
					if [ "$#" -gt 0 ]
					then 
						pkg_group="$@"
						shift $#  # Shift the amount of arguments.
					else echo "ERR: Please specify one or more package groups:" ; echo "$available_package_groups" ; exit 1
					fi ;;
			esac
			;;
		*) break ;;
	esac
done

[ "$pkg_group" ] || { echo "ERR: Missing -p | --pkg_group flag. Use it to specify one or more package groups:" ; echo "$available_package_groups"  ; exit 1 ; }

# This was the GPG verification part, but...
# https://github.com/NixOS/nix/pull/7411 - "...and probably few people care about GPG signatures anyway."
## We need this because for some stupid reason https://nixos.org/nix/install.asc does not redirect to https://releases.nixos.org/nix/nix-version/install.asc
#nix_version="$(wget -S https://nixos.org/nix/install 2>&1 | grep 'Location' | head -n 1 | cut -d '/' -f 5 | cut -d '-' -f 2)"
#
#mkdir -p ~/nix_inst || { echo 'ERR: Could not create ~/nix_inst/ directory!' ; exit 1 ; }
#cd ~/nix_inst
#wget \
#	https://nixos.org/nix/install \
#	https://releases.nixos.org/nix/nix-"$nix_version"/install.asc \
#	https://releases.nixos.org/nix/nix-"$nix_version"/install.sha256 \
#	https://nixos.org/edolstra.gpg
#gpg --import edolstra.gpg
#gpg --verify install.asc || { echo 'ERR: Could not verify the Nix install script!' ; exit 1 ; }

wget https://nixos.org/nix/install
$privprefix mkdir -m 0755 /nix/ && $privprefix chown "$USER" /nix/
chmod +x install && 
if [ "$mode" -eq 2 ]
	then ./install --daemon    # https://nixos.org/manual/nix/stable/installation/multi-user.html
	else ./install --no-daemon # https://nixos.org/manual/nix/stable/installation/single-user.html
fi

# Delete the last line which was just appended to ~/.profile by Nix since that line is already there.
sed -i '$d' ~/.profile

# Install packages declared in ~/.config/nixpkgs/config.nix
/home/$USER/.nix-profile/bin/nix-env --install --attr $pkg_group


# This is necessary for stuff like this to work: mpv https://youtube.com/watch?v=dQw4w9WgXcQ
ln -s /home/"$USER"/.nix-profile/bin/yt-dlp /home/"$USER"/.local/bin/youtube-dl


# And now for some general stuff which was present in prepare_ubuntu.bash
for group in \
	audio \
	libvirt \
	wheel \
	wireshark \
	;
do $privprefix addgroup "$group" && $privprefix addgroup "$USER" "$group" ; done

# Populate ~/sources
src="/home/$USER/sources/"
if [ -d $src ] ; then cd $src ; else mkdir -p $src && cd $src ; fi
for url in \
	https://git.tartarus.org/simon/xcopy.git \
	https://github.com/graupe/brownout \
	https://github.com/macvk/dnsleaktest \
	;
do git clone --depth=1 $url ; done
