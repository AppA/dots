#!/usr/bin/env sh

# Calculate total duration of audio and/or video files (in a path (optionally recursive))
# OG source: https://unix.stackexchange.com/a/170973

f__print_usage () {
cat << EOF
Usage: ${0##*/} [[-b|--backend] mediainfo | ffprobe | exiftool] [-r|--recursive] path...

Paths can be either files or directories.
You can specify the recursion flag per path.
Example: ${0##*/} dir1 dir2 -r dir3 dir4 file1 file2 -r dir5
In this example, only dir3 and dir5 will be recursed into.
EOF
}

if [ -z "$1" ] ; then f__print_usage ; exit 1 ; fi

f__set_backend () {
backend="$1"
case "$backend" in
	*mediainfo*) f__get_duration () { mediainfo --Inform="General;%Duration%" "$1" ; } ;;
	*ffprobe*  ) f__get_duration () { ffprobe -v quiet -of csv=p=0 -show_entries format=duration "$1" | awk '{print $1 * 1000}' ; } ;;
	*exiftool* ) f__get_duration () { exiftool -Duration "$1" | awk '{ split ($3, t, ":") ; print (t[1] * 3600 + t[2] * 60 + t[3]) * 1000 }' ; } ;;
	*)
		echo "WARN: No such backend ($backend), trying an alternative backend..." >&2
		f__set_backend "$(command -v mediainfo || command -v ffprobe || command -v exiftool)"
		;;
esac
}

# Argument / flag logic
while [ $# -gt 0 ] ; do
	case $1 in
		-b|--backend) # This flag must take another argument.
			if [ -n "$2" ] && [ -n "${2%%-*}" ]
				then
					backend="$2"
					case "$(command -v $backend)" in
						*mediainfo) f__set_backend "$backend" ;;
						*ffprobe  ) f__set_backend "$backend" ;;
						*exiftool ) f__set_backend "$backend" ;;
										* ) echo "WARN: No such backend ($backend), trying an alternative backend..." >&2 ; f__set_backend ;;
					esac
				shift 2
				else echo "ERR: Must specify the backend command (mediainfo / ffprobe / exiftool)" ; exit 1
			fi ;;
		*) [ -z "$backend" ] && f__set_backend mediainfo ; break ;; # mediainfo was chosen as default because it performed the fastest.
	esac
done

# Function to process paths (files or directories)
f__process_path () {
path="$1"
recursive="$2"
if [ -f "$path" ]
	then f__get_duration "$path"
	elif [ -d "$path" ]
		then
			if [ "$recursive" = 1 ]
				then # If the path is a directory and recursive flag is set, find and process files recursively.
					find "$path" -type f | while read find_output ; do f__get_duration "$find_output" ; done # https://stackoverflow.com/a/67780632
				else # If the path is a directory and recursive flag is not set, process files in the directory non-recursively.
					for file in "$path"/* ; do
						if [ -f "$file" ] ; then f__process_path "$file" 0 ; fi
					done
			fi
	else echo "ERR: $path is not a valid file or directory." >&2 ; exit 1
fi
}

# Collect durations and process arguments.
while [ $# -gt 0 ] ; do
	case $1 in
		-r|--recursive)
			recursive=1
			shift
			if [ -z "$1" ] ; then f__print_usage ; exit 1 ; fi
			;;
		*)
			output=$(f__process_path "$1" $recursive)
			durations="${durations}${output}"$'\n'
			recursive=0  # Reset recursive flag after processing each path.
			shift ;;
	esac
done

echo "$durations" | awk '
BEGIN { sum = 0 }
{
	if ($1 != "") sum += $1 / 1000  # Convert milliseconds to seconds
}
END {
	days = int(sum / 86400)
	hours = int((sum % 86400) / 3600)
	minutes = int((sum % 3600) / 60)
	seconds = int(sum % 60)
	printf("%dd:%02dh:%02dm:%02ds\n", days, hours, minutes, seconds)
}'
