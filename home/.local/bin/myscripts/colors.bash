#!/usr/bin/env bash

# Imported from bashlib
# https://github.com/lhunath/scripts/blob/master/bashlib/bashlib

# Make sure to source this file instead of executing it!

#  ______________________________________________________________________ 
# |                                                                      |
# |                                          .:  GLOBAL DECLARATIONS  :. |
# |______________________________________________________________________|

## Variables for convenience sequences.
#bobber=(     '.' 'o' 'O' 'o' )
#spinner=(    '-' \\  '|' '/' )
#crosser=(    '+' 'x' '+' 'x' )
#runner=(     '> >'           \
#             '>> '           \
#             ' >>'           )

# Variables for terminal requests.
[[ -t 2 && $TERM != dumb ]] && {
#    COLUMNS=$({ tput cols   || tput co;} 2>&3) # Columns in a line
#    LINES=$({   tput lines  || tput li;} 2>&3) # Lines on screen
#    alt=$(      tput smcup  || tput ti      ) # Start alt display
#    ealt=$(     tput rmcup  || tput te      ) # End   alt display
#    hide=$(     tput civis  || tput vi      ) # Hide cursor
#    show=$(     tput cnorm  || tput ve      ) # Show cursor
#    save=$(     tput sc                     ) # Save cursor
#    load=$(     tput rc                     ) # Load cursor
#    dim=$(      tput dim    || tput mh      ) # Start dim
    bold=$(     tput bold   || tput md      ) # Start bold
#    stout=$(    tput smso   || tput so      ) # Start stand-out
#    estout=$(   tput rmso   || tput se      ) # End stand-out
#    under=$(    tput smul   || tput us      ) # Start underline
#    eunder=$(   tput rmul   || tput ue      ) # End   underline
    reset=$(    tput sgr0   || tput me      ) # Reset cursor
#    blink=$(    tput blink  || tput mb      ) # Start blinking
#    italic=$(   tput sitm   || tput ZH      ) # Start italic
#    eitalic=$(  tput ritm   || tput ZR      ) # End   italic
[[ $TERM != *-m ]] && {
    red=$(      tput setaf 1|| tput AF 1    )
    green=$(    tput setaf 2|| tput AF 2    )
    yellow=$(   tput setaf 3|| tput AF 3    )
    blue=$(     tput setaf 4|| tput AF 4    )
    magenta=$(  tput setaf 5|| tput AF 5    )
    cyan=$(     tput setaf 6|| tput AF 6    )
}
    black=$(    tput setaf 0|| tput AF 0    )
    white=$(    tput setaf 7|| tput AF 7    )
    default=$(  tput op                     )
#    eed=$(      tput ed     || tput cd      )   # Erase to end of display
#    eel=$(      tput el     || tput ce      )   # Erase to end of line
#    ebl=$(      tput el1    || tput cb      )   # Erase to beginning of line
#    ewl=$eel$ebl                                # Erase whole line
#    draw=$(     tput -S <<< '   enacs
#                                smacs
#                                acsc
#                                rmacs' || { \
#                tput eA; tput as;
#                tput ac; tput ae;         } )   # Drawing characters
#    back=$'\b'
#} 3>&2 2>/dev/null ||:
}
