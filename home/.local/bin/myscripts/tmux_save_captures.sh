#!/usr/bin/env sh

# Captures + saves a snapshot of all tmux sessions, windows and panes into a specified directory.

[ -z "$1" ] && echo "Usage: $0 <directory>" && exit 1
command -v tmux >/dev/null || { echo "ERR: tmux not found. Please install tmux." ; exit 1 ; }

snapshot_dir="$1"
mkdir -p "$snapshot_dir"

# List all sessions
tmux list-sessions -F "#{session_name}" | while read session ; do
	tmux list-windows -t "$session" -F "#{window_index}:#{window_name}" | while read window_line ; do
		window_index="$(echo "$window_line" | cut -d: -f1)"
		window_name="$(echo "$window_line" | cut -d: -f2-)"
		# Sanitize window names so that they don't contain illegal filename characters when saving as file.
		sanitized_window_name="$(echo "$window_name" | sed -e 's|[^A-Za-z0-9._-]|_|g')"
		tmux list-panes -t "$session:$window_index" -F "#{pane_index}" | while read pane ; do
				# Capture the contents of each pane (including scrollback, the value of which should be large enough for most usecases)
				tmux capture-pane -t "$session:$window_index.$pane" -S -100000000 -E -1 -p > "$snapshot_dir/$session-$window_index-$sanitized_window_name-$pane.txt"
		done
	done
done

echo "ANN: tmux snapshots have been saved to $snapshot_dir"
