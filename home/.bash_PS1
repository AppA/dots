#!/bin/bash

# This file is mainly used for Bash shell prompt (PS1) configuration.
# I've heavily commented this so that people looking to customize their PS1 will hopefuly have an easier time than I did :)
# Reference material for further reading - http://www.gnu.org/software/bash/manual/bashref.html#Controlling-the-Prompt
# Thanks to all the knowledgeable people in #bash @ irc.freenode.net

# TODO: Finish commenting the PS1. Mainly focus on explaining what non-printing character sequence indicators are.

# Non-UTF8 prompt. Use this when terminal does not support UTF8.
#PS1="\n\A \[$green\]\[$bold\]\u\[$reset\]@\[$blue\]\H\[$reset\]:\l: \[$bold\]\[$cyan\]\w\[$reset\] [\$?]\[$bold\]\$(__git_ps1)\[$reset\] \n\$ "


##############
# THE PROMPT #
##############


# We wrap the definition of the actual PS1 inside a function, so that it can be called later by a git status wrapper (see f__git_prompt_wrapper below)
f__define_PS1 () {

## Working with arrays.
# We will be relying heavily on Bash arrays to set the color of various things in our PS1. The color will be dependent on a few things, so this is why using arrays gives us greater control and is generally easier to manage.
# As an introduction, it's recommended to read this first - http://mywiki.wooledge.org/BashGuide/Arrays
# Furthermore, since arithmetic expressions are supported, you should also get yourself a bit familiar with this - http://www.gnu.org/software/bash/manual/bashref.html#Shell-Arithmetic
# We'll be practically explaining the arithmetic part, so it's ok if you don't understand most of it yet.

# This is for colors depending on UID.
# ${uid_color[UID!=0]}
uid_colors=( "$bold$red" "$bold$cyan" "$bold$green")

# These are the colors we can choose from for coloring the hostname. 
# I need this because when I have a shell on a remote machine, I would like to see it indicated somewhere in the prompt.
hostname_colors=( "$blue" "$yellow" )

# Here I specify an array of colors used to give a non-0 job indicator a color.
# So that means that when there are some jobs in the background, the job indicator will get colored.
job_colors=( [0]="$bold$yellow" )

exit_colors=( [0]="$green" [1]="$red" [2]="$yellow" )
# $? <= 1 ? $? : 2
# if (($?<=1)) then take $?; otherwise take 2
# $? == 0 ---> 0
# $? == 1 ---> 1
# $? == 2 ---> 2
# $? == something greater than 2 ---> 2

## And now... for the PS1!
#
# The coloring will depend on a couple of things:
	# Whether the user has root privileges or not.
	# Whether the user is logged in over SSH.
	# What the exit status is.
	# Whether there are any background jobs or not.
# Do `sudo bash` and then see the prompt change. (This however might not work on many Red Hat based systems, primarily because of SELinux isolation)
# The reason you see `PS1+` variables definitions is that you can also concatenate strings - http://stackoverflow.com/a/4182643

# You're also probably wondering why some variables are inside the \[ \] thingies. 
# Those are called the non-printing character sequence indicators. These are bash prompt escape sequences.
# More info:
# http://mywiki.wooledge.org/BashFAQ/053
# http://unix.stackexchange.com/a/157966/136906

PS1='\n┌───| ' # The `\n` character that you see there, is called a backslash-escaped special character. This one tells us to put the cursors at a new line.
PS1+='\[${uid_colors[UID==0?0:1]}\]\w\[$reset\] ]' # The \w here is the current working path. This backslash-escaped special character should have the similar contents as when you invoke the `pwd` command.
PS1+='(\[${uid_colors[UID==0?0:2]}\]\u\[$reset\]' # Here we do exactly the same but with different color and with username and hostname. We also put then inside parentheses for cosmetics.
PS1+='@' # Just a visual aid so that we get user@hostname.
PS1+='\[${hostname_colors["$ISREMOTE"==1?1:0]}\]\H\[$reset\])' # Color hostname yellow if the var SSH_CLIENT is assigned. See ~/.bashrc.

# Here we are going to display whatever $SHELL expands to if it's anything other than /bin/bash.
if [[ $SHELL != /bin/bash ]] # Notice that the $SHELL variable is not quoted, this is because the `[[` test doesn't do word splitting or pathname expansion.
then
	PS1+='[$SHELL]' # This is simply the $SHELL variable that you can echo during interactive shell sessions. The square brackets are pure for cosmetics too.
fi

PS1+='|[\[$white\]\A\[$reset\]]' # The time indicator (according to HH:MM formatting) in white color.
PS1+='[p:$BASHPID]' # This environment variable is used to tell us what process ID the shell instance has.
PS1+='[bn:\l]' # An indicator for the basename of the shell’s terminal device name.

PS1+='\[${job_colors[\j==0]}\][j:\j]\[$reset\]' # Let's break this down a bit.
	# You have the ${job_colors[\j==0]} array operation encased inside the non-printing character sequence indicators ( the \[ \] thingies, more info here - http://mywiki.wooledge.org/BashFAQ/053 )
	# You can do arithmetic tests within bash arrays. In our case, this means that when the amount of background jobs equals 0 (\j==0), the outcome of the test will be equal to 'true'. 
	# When outcome of the test is equal to true, it will choose the first item in the array (in this case the $bold$yellow we have specified in the job_colors array.
	# To not color any further than we need, we reset the color by issuing \[$reset\].
# TODO: Only display jobs (colored) if there are any jobs.
#if [[ -n "$(jobs)" ]]
#then
#	#PS1+='\[${job_colors[\j==0]}\][j:\j]\[$reset\]' # Let's break this down a bit.
#	# You have the ${job_colors[\j==0]} array operation encased inside the non-printing character sequence indicators ( the \[ \] thingies, more info here - http://mywiki.wooledge.org/BashFAQ/053 )
#	# You can do arithmetic tests within bash arrays. In our case, this means that when the amount of background jobs equals 0 (\j==0), the outcome of the test will be equal to 'true'. 
#	# When outcome of the test is equal to true, it will choose the first item in the array (in this case the $bold$yellow we have specified in the job_colors array.
#	# To not color any further than we need, we reset the color by issuing \[$reset\].
#	PS1+='\[$bold$yellow\][j:\j]\[$reset\]'
#fi

PS1+='► [\[${exit_colors[$?<=1?$?:2]}\]$?\[$reset\]]' # Exit status color colored by the $exit_colors array. Let's break this down too:
	# The main thing we like to point out is this part right here: $?<=1?$?:2
	# This means: if (($?<=1)) then take $?; otherwise take 2
	# So if $? (exit status) is smaller or equal to 1, then take whatever number the exit status indicator is telling us as the array index item number. 
	# In our case, if exit status would be 0, it would thus be $green (item 0 in the array) and if exit status would be 1, then naturally it would be the same as item 1 in the array ($red).
	# If, however, the exit status is not 0 nor 1, then choose the 2nd item in the array index ($yellow).
if declare -f __git_ps1 >/dev/null 2>&1 # This is why we need to wrap this entire thing inside of a function.
	then PS1+='\[$bold\]$(__git_ps1)\[$reset\] ' # The __git_ps1 function is sort of a PS1 for git repos and it comes from git-prompt.sh - Works only if you have git installed.
	else PS1+='\[$bold\]\[$reset\] ' # Without the special git indicator.
fi
PS1+='\n└─\$ ' # End it with a newline again, so that you don't have the cursor next to the prompt but under it. Also it has the \$ prompt indicator at the end, which changes the prompt to $ when you're a normal user and # when you're the root user.

}


f__git_prompt_wrapper () {
if git rev-parse --git-dir >/dev/null 2>&1
	then
		# Apparently the contents of prompt.sh can also be found in /usr/lib/git-core/git-sh-prompt ?
		# ...or at https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh ?
		source ~/.local/bin/myscripts/git-prompt.sh
		# Some options for git-prompt.sh
		GIT_PS1_SHOWDIRTYSTATE=1
		GIT_PS1_SHOWSTASHSTATE=1
		GIT_PS1_SHOWUNTRACKEDFILES=1
		GIT_PS1_SHOWCOLORHINTS=1 # Set color.
		GIT_PS1_DESCRIBE_STYLE="branch"
		GIT_PS1_SHOWUPSTREAM="auto git"
		f__define_PS1
	else
		f__define_PS1
fi
}

# https://stackoverflow.com/questions/3058325/what-is-the-difference-between-ps1-and-prompt-command
PROMPT_COMMAND=f__git_prompt_wrapper
