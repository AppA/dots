;; Ok, the next block is just some custom stuff I shouldn't touch.
;; Maybe bring it to another file with a variable?
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(tango-dark))
 '(custom-safe-themes
   '("fe1c13d75398b1c8fd7fdd1241a55c286b86c3e4ce513c4292d01383de152cb7" default))
 '(helm-completion-style 'emacs)
 '(org-agenda-files
   '("~/Documents/from_phone/sync-phone/orgfiles/mainorgmodefile.org"))
 '(org-archive-default-command #'prov/org-archive-subtree-hierarchically)
 '(org-hide-leading-stars t)
 '(package-selected-packages '(ement beacon dash evil which-key helm use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Ubuntu Mono" :foundry "DAMA" :slant normal :weight normal :height 97 :width normal)))))



;;;; My Stuff 



;; Reload Emacs config from within Emacs. Thanks @provessor:matrix.org :)
(defun mystuff--reload-config ()
  "Reload config from within Emacs."
  (interactive) ; can be called from a keybind or M-x
  (load (expand-file-name "init" user-emacs-directory)))
;; Could also be added to a keybind, but I choose not to here
;(global-set-key (kbd "convoluted keybind") #'your-reload-config-name)


(require 'package)
;;(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;;(add-to-list 'package-archives '("melpa" . "http://stable.melpa.org/packages/"))
;; Below instead of the 2 lines above because: https://git.sr.ht/~marcuskammer/emacs.d/tree/main/item/bundle/bundle--package.el#L4
;; Also: https://github.com/jwiegley/use-package#getting-started
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package org
  :ensure t)
(require 'org-archive)

(use-package which-key
  :ensure t
  :init
  (which-key-mode))

(use-package evil
   :ensure t)
(evil-mode 1)

   
;; Source: https://github.com/matman26/emacs-config/blob/master/myinit.org#helm-for-navigation
(use-package helm
  :init
    (require 'helm-config)
    (setq helm-split-window-in-side-p t
          helm-move-to-line-cycle-in-source t)
  :config 
    (helm-mode 1) ;; Most of Emacs prompts become helm-enabled
    (helm-autoresize-mode 1) ;; Helm resizes according to the number of candidates
	(global-set-key (kbd "C-x C-f") 'helm-find-files) ;; Finding files with Helm
	(global-set-key (kbd "C-s") 'helm-occur)  ;; Replaces the default isearch keybinding
	(global-set-key (kbd "M-x") 'helm-M-x)  ;; Improved M-x menu
	(global-set-key (kbd "M-y") 'helm-show-kill-ring)  ;; Show kill ring, pick something to paste
  :ensure t)


;; Here I'm defining a function which copies URLs into the selection buffer.
(defun mystuff--kill-url (url &rest _ignore) (kill-new url))
;; Then, I set browse-url-browser-function variable to that function, so every time the URL gets clicked / followed, this function will be called.
(setq browse-url-browser-function 'mystuff--kill-url)

(use-package dash
  :ensure t)

(defun prov/org-archive-subtree-hierarchically (&optional prefix)
  (interactive "P")
  (let* ((fix-archive-p (and (not prefix)
                             (not (use-region-p))))
         (afile  (car (org-archive--compute-location
                       (or (org-entry-get nil "ARCHIVE" 'inherit) org-archive-location))))
         (buffer (or (find-buffer-visiting afile) (find-file-noselect afile))))
    (org-archive-subtree prefix)
    (when fix-archive-p
      (with-current-buffer buffer
        (goto-char (point-max))
        (while (org-up-heading-safe))
        (let* ((olpath (org-entry-get (point) "ARCHIVE_OLPATH"))
               (path (and olpath (split-string olpath "/")))
               (level 1)
               tree-text)
          (when olpath
            (org-mark-subtree)
            (setq tree-text (buffer-substring (region-beginning) (region-end)))
            (let (this-command (inhibit-message t)) (org-cut-subtree)) ; we don’t want to see "Cut subtree" messages
            (goto-char (point-min))
            (save-restriction
              (widen)
              (-each path
                (lambda (heading)
                  (if (re-search-forward
                       (rx-to-string
                        `(: bol (repeat ,level "*") (1+ " ") ,heading)) nil t)
                      (org-narrow-to-subtree)
                    (goto-char (point-max))
                    (unless (looking-at "^")
                      (insert "\n"))
                    (insert (make-string level ?*)
                            " "
                            heading
                            "\n"))
                  (cl-incf level)))
              (widen)
              (org-end-of-subtree t t)
              (org-paste-subtree level tree-text))))))))

;; Temporary Evil-mode vs. Org-mode v28 workaround
;; See: https://teddit.net/r/emacs/comments/uai4gq/disastrous_upgrade_to_281_orgmode_header/
;; And: https://github.com/emacs-evil/evil/issues/1604
(with-eval-after-load 'evil-maps (define-key evil-motion-state-map (kbd "TAB") nil))


;; Always follow symlinks
;; This is primarly because when I start up Emacs and do "C-x r b", it asks me whether I want to follow the symlink.
;; Source: https://stackoverflow.com/questions/15390178/emacs-and-symbolic-links
(setq vc-follow-symlinks t)


;;;;;;;;;;;;;;;;;;;;;
;;;;; Cosmetics ;;;;;
;;;;;;;;;;;;;;;;;;;;;

;; (global-hl-line-mode t) ;; This highlights the current line in the buffer

;; (use-package dracula-theme
;;    :config
;;    (load-theme 'dracula)
;;    :ensure t)

(use-package beacon ;; This applies a beacon effect to the highlighted line
   :ensure t
   :config
   (beacon-mode 1))

(set-background-color "black")


;; Disable bold fonts
;; From https://stackoverflow.com/questions/3375088/disable-italic-in-emacs
(set-face-bold-p 'bold nil)

;;;;;;;;;;;;;;;;;;;;;
;;;; / Cosmetics ;;;;
;;;;;;;;;;;;;;;;;;;;;
