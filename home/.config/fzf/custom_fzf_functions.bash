##### My custom fzf functions
# These should only get sourced if fzf is available. See ~/.config/fzf/fzf.bash


#### Select command from chosen history.
__fzf_chosen_history__ () { cat ~/.bash_history_chosen ~/.bash_history_chosen_s | fzf --reverse --no-mouse --height "40%" --color='fg:250' ;}


#### Use __fzf_cd__ (usually mapped to Alt+c) but also return dot-dirs as results.
# TODO: Make this work for dirs with spaces in their names.
CDD () { 
	local FZF_ALT_C_COMMAND="find . -type d" ; $(__fzf_cd__)
}
