# Setup fzf
# ---------
# Add this dir in my $PATH if I've install fzf from source / git.
if [[ -f "$HOME/sources/fzf/bin/fzf" ]] && [[ ! "$PATH" == *$HOME/sources/fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}$HOME/sources/fzf/bin"
fi

# Auto-completion
# ---------------
if [[ $- == *i* ]]
	then
		if [[ -f $HOME/sources/fzf/shell/completion.bash ]] ; then source $HOME/sources/fzf/shell/completion.bash 2> /dev/null ; fi
		if [[ -f ~/.config/fzf/completion.bash ]] ; then source ~/.config/fzf/completion.bash 2> /dev/null ; fi
fi

# Sourcing stuff depending whether fzf is installed.
# ------------
if command -v fzf > /dev/null
then 
	source "$HOME/.config/fzf/custom_fzf_functions.bash"  # Custom functions only get sourced if fzf is available.
	source "$HOME/.config/fzf/key-bindings.bash"  # Keybindings only get sourced if fzf is available.
fi



# Options
# ------------
export FZF_DEFAULT_OPTS='--layout=reverse --border --no-mouse --multi --black --color="fg:242"'
