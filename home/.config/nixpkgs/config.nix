# This is supposed to be ~/.config/nixpkgs/config.nix
# Mostly taken from https://nixos.org/manual/nixpkgs/stable/#sec-declarative-package-management
# To install base packages: nix-env -iA nixpkgs.packages__base
# To install base and media packages: nix-env -iA nixpkgs.packages__base nixpkgs.packages__media
# New package groups specified here MUST be in the format: packages__name
#
{
  packageOverrides = pkgs: with pkgs; rec {
    # Specify nixGL here declaratively instead of the recommended nix-env method.
    nixGL =
      let
       nixGLtarball = fetchTarball "https://github.com/guibou/nixGL/archive/main.tar.gz";
      in import nixGLtarball { inherit pkgs ; } ;
    myProfile = writeText "my-profile" ''
      export PATH=$HOME/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/sbin:/bin:/usr/sbin:/usr/bin
      export MANPATH=$HOME/.nix-profile/share/man:/nix/var/nix/profiles/default/share/man:/usr/share/man
      # glibcLocales "hack". This makes it so locales can be used across all distros.
      export LOCALE_ARCHIVE=${pkgs.glibcLocales}/lib/locale-archive
      '';
    packages__base = pkgs.buildEnv {
      name = "Base-packages";
      paths = [
        (runCommand "profile" {} ''
          mkdir -p $out/etc/profile.d
          cp ${myProfile} $out/etc/profile.d/my-profile.sh
        '')
        aria
        bash-completion
        btop
        curl
        fzf
        git
        glibcLocales
        gnupg
        jq
        lnav
        macchanger
        man
        mtr
        ncdu
        nettools
        perl
        pmutils
        sshfs
        sshuttle
        stress-ng
        syncthing
        tcpdump
        tmux
        translate-shell
        tree
        tshark
        vifm
        #vim
        wavemon
        yt-dlp
      ];
    };
    packages__media = pkgs.buildEnv {
      name = "Media-packages";
      paths = [
        #ffmpeg
        mediainfo
        motion
        mpd
        #mpv
        pulsemixer
      ];
    };
    packages__GUI = pkgs.buildEnv {
      name = "GUI-packages";
      paths = [
        alacritty
        arandr
        i3
        i3-layout-manager
        i3status
        #nixGL.auto.nixGLDefault
        rofi
        scrot
        #slock
        st
        sxiv
        virt-manager
        xclip
        xdotool
        xorg.setxkbmap
        xorg.xinput
        xorg.xmodmap
        xorg.xset
      ];
    };
    extraOutputsToInstall = [ "man" "doc" ];
  };
}
