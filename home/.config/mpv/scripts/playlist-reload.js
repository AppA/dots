// ----- RELOAD PLAYLIST: script-message playlist-reload -----
// Source: avih in #mpv (Libera.chat)
function make_m3u(playlist) {
    var rv = "#EXTM3U\n"
    playlist.forEach(function(i) {
        if (!i.filename)
            return;
        rv += "\n";
        if (i.title)
            rv += "#EXTINF:0," + i.title + "\n";
        rv += i.filename + "\n";
    });
    return rv;
}

// reload current playlist and sets the same current playlist item (in case
// write-config failed because unseekable).
// if write-watch-later-config is used before -> will also restore file position
var wtid = null;
mp.register_script_message("playlist-reload", function() {
    var count = mp.get_property_number("playlist-count");
    var current = mp.get_property_number("playlist-pos");
    if (!(count >= 1 && current >= 0))
        return;

    clearTimeout(wtid);
    wtid = setTimeout(function() {  // let write-watch-later-config complete if invoked
        var m3u = make_m3u(mp.get_property_native("playlist"));
        mp.commandv("loadlist", "memory://" + m3u, "replace");
        mp.set_property("playlist-pos", current);
    }, 500);
});
