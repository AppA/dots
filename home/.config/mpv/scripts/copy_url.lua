-- An mpv script to copy URLs into the window system clipboard and tmux
-- Supports both X and Wayland (and tmux :))
-- A more thorough version of this script, minus the tmux support: https://0x0.st/XORJ.txt (thanks to sfan5 @ Libera.chat)

function copy_url()
	local url = mp.get_property("path")
	if url then
		-- Escape single quotes in the URL
		url = string.gsub(url, "'", "'\\''")

		-- Copy to X clipboard (requires: xclip)
		os.execute(string.format("echo -n '%s' | xclip -selection clipboard", url))
		-- Copy to Wayland clipboard (requires: wl-clipboard)
		os.execute(string.format("echo -n '%s' | wl-copy", url))
		-- Copy to tmux buffer (requires: tmux)
		os.execute(string.format("tmux set-buffer -w '%s'", url))

		mp.osd_message("copy_url.lua: URL copied (to clipboard and/or tmux)")
	else
		mp.osd_message("copy_url.lua: No URL found")
	end
end

mp.register_script_message("copy_url", copy_url)
