# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# If actually running Bash, and ~/.bashrc is readable, then source it.
if [ -n "$BASH" ] && [ -r ~/.bashrc ]
	then . ~/.bashrc
fi



## Variable exports

# If it's not already present in /etc/profile(.d) then manually export this var:
export XDG_CONFIG_HOME="$HOME/.config"

# Locale definition. So far seems like C.UTF-8 works with Nixpkgs without breakage and I have not yet seen other breakage (please lord don't let me jinx this)
# This locale stuff is apparently pretty tricky too: https://news.ycombinator.com/item?id=15681351
export LC_ALL=C.UTF-8



## (Custom) $PATH locations

# In bash 4, when the globstar shell option is enabled ( shopt -s globstar ), the glob ** expands to all files and directories found recursively under the current directory, and **/ to all directories. Combinations like foo/**/*.txt work, but **.txt and foo** do not.
shopt -s globstar
for d in ~/.local/bin/**/ ; do PATH+=:${d%/} ; done  # So with globstar enabled, this enables scripts to run which are located in (sub)directories.
shopt -u globstar

[ -d ~/.pyenv/bin/ ] && PATH="$HOME/.pyenv/bin/:$PATH"
[ -d ~/.cargo/bin/ ] && PATH="$HOME/.cargo/bin/:$PATH"

# Nix profile inclusion.
[ -e ~/.nix-profile/etc/profile.d/nix.sh ] && . ~/.nix-profile/etc/profile.d/nix.sh

# rustup / cargo
[ -e ~/.cargo/env ] && . ~/.cargo/env
