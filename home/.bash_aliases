#######################
### --- ALIASES --- ###
#######################

# Location ~/.bash_aliases
# If you want an un-aliased command, simply type \command

# Temporary aliases
if [ -f ~/.bash_aliases_tmpr ] ; then source ~/.bash_aliases_tmpr ; fi


## Main aliases

alias sudo='sudo ' # http://askubuntu.com/q/22037

alias ..='cd ..'
alias gri='grep -i'
alias srb='source ~/.bashrc'
alias rm='rm -v'
alias mv='mv -vi'
alias p='ping'
alias bc='bc -l' # Include mathlib
alias v='vim'
alias vps='sh ~/.local/bin/myscripts/vim_process_swap.sh'
alias mnt='mount | column -t' # Currently mounted filesystems in nice layout (from commandlinefu)
alias ets='echo "Timestamp: $(date +%F__%T%z__%a)"'
alias upd='sudo -v ; sudo apt update && sudo apt -V upgrade ; echo ; ets'
alias sy='rsync --recursive --links --perms --times --devices --specials --verbose --partial --progress --human-readable' # Requires rsync version >=3.1.0 (see --info=FLAGS in man page)
alias syp='sy --group --owner'
alias we='wget'
alias wet='wget --trust-server-names'
alias wes='wget --spider' # Tells me filesize of remote file.
alias d='du -sh'
alias dt='dtpwd="$(pwd | sed "s|^/home/$USER|~|")" ; tmux set-buffer -w "$dtpwd" ; echo "$dtpwd" | xclip -sel clip 2>/dev/null' # Put relative current working dir into tmux + xclip buffer.
alias dtf='tmux set-buffer -w "$PWD"' # Put absolute current working dir into tmux + xclip buffer.
alias cal='ncal -3 -M' # ncal view, 1mo before + 1mo after, week starts on Monday.
alias pp='ps -auxwww | head -n 1 ; ps -auxwww | grep -v grep | grep -i'

alias s1='sha1sum'
alias s2='sha256sum'
alias s5='sha512sum'

alias mp='mpv --pause'
alias mpl='mp --load-unsafe-playlists'

alias tminit='tmux_template_admin.sh ; tmux_template_media.sh ; tmux_template_comms.sh ; tmux_template_daem.sh ; tmux_template_logstats.sh -i wlp3s0 ; tmux_start_logging.sh'

# i3 stuff
alias i3sus='$privprefix true && slock $privprefix pm-suspend' # Execute slock before suspending the machine.
alias i3r='sh ~/.local/bin/myscripts/guistuff/init_x_config.sh ; bro 223 ; sh ~/.local/bin/myscripts/guistuff/backlightctl.sh'

# Toggle Intel Turbo Boost on/off (requires root and is hardware dependant, even among Intel machines)
alias T0='$privprefix sh -c "echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo" && echo "Intel Turbo-Boost disabled!"' # off
alias T1='$privprefix sh -c "echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo" && echo "Intel Turbo-Boost enabled!"' # on

# Git specific aliases
alias gs='git status'
alias gd='git diff'
alias ga='git add'

# Enable color support of ls and also add some handy aliases.
if [ -x /usr/bin/dircolors ]
	then
		test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
		alias ls='ls --color=auto'
		#alias dir='dir --color=auto'
		#alias vdir='vdir --color=auto'

		alias grep='grep --color=auto'
		alias fgrep='fgrep --color=auto'
		alias egrep='egrep --color=auto'
fi


# Some more ls aliases.
# On some systems, you have to add `; complete -r <alias>` at the end of the alias because of "programmable completion interpreting aliases".
lshelp="$(ls --help 2>&1)"
if echo "$lshelp" | grep -qi coreutils
	then
		alias l='ls -l --human-readable --classify --show-control-chars --group-directories-first --color=auto --time-style=long-iso' # Long listing of non-hidden files and dirs.
		alias ll='l --all' # Long listing of all files and dirs (incl. dotfiles)
		alias lt='l -tc' # Sorts on latest modified / created time.
		alias ldf='ll --directory .*' # Show only hidden files and dirs.
		alias llt='ls -l --all --human-readable --classify --show-control-chars --color=auto --time-style=long-iso -tc' # Same as lt (but incl. dotfiles)
		alias lth='llt --color=always | head' # Print latest modified files/dirs and preserve color.
elif echo "$lshelp" | grep -qi busybox
	then
		alias l='ls -lhF --group-directories-first --full-time --color=auto'
		alias ll='l -a'
		alias lt='l -tc'
		alias ldf='ll -d .*'
		alias llt='ls -lahF --full-time --color=auto -tc'
		alias lth='llt --color=always | head'
	else # BSD
		alias l='ls -lhFT'
		alias ll='l -a'
		alias lt='l -tc'
		alias ldf='ll -d .*'
		alias llt='ls -lahFT -tc'
		alias lth='llt | head' # No colors :(
fi


# Alias for https://termbin.com/
# Usage: something-that-generates-output | tb
# As of 2020-05-05__03:05:02+0200__Tue: Life span of single paste is one month. Older pastes are deleted.
alias tb='nc termbin.com 9999'

# Brownout (redshift replacement). Usage: bro 203
alias bro='~/sources/brownout/brownout' # http://www.wired.com/2013/09/flux-eyestrain/ - it's too late anyway, but whatever.


alias tlate='~/sources/translate-shell/translate'



#########################
### --- FUNCTIONS --- ###
#########################
# Sorted roughly by their complexity.
# TODO: Add functionality of only listing personal functions.


#### Recursively find filenames using find command from the current working dir.
# TODO: Add possibility to add path argument, and if not present, assume current working dir.
# Here I used "*$**" instead of "*$@*" because - http://wtfsh.kpaste.net/205d
f () { find . -iname "*$**" ;}
fx () { find . -xdev -iname "*$**" ;}  # Same but don't traverse other filesystems.


#### Recursively find search string within contents of filenames in current working dir using grep.
# TODO: See same TODO as function f.
g () { grep -Ril "$*" . ;}


#### Supertype. Not only executes type on the command but also does an ls of its location.
t () { type -a "$1" | tee /dev/tty | ls -lahF "$(awk '/\// { print $3 }')" ;}


#### Perform llt after changing working directory.
cdl () { builtin cd "$1" && llt ;}


#### Make a directory and then cd into it.
# TODO: Make it so that it can also work on directories with spaces in it.
mcd () { mkdir -p "$1" && eval cd "$1" ;}


#### Print local connections based on network device
# Usage: `ploccon wlan`
ploccon () {
sudo netstat -tupwn | grep "$(ip -oneline addr | grep "$1" | awk '{print $4}' | cut --delimiter="/" --fields 1)"
}


#### Temperature conversion (Fahrenheit <> Celcius)
# scale=2 makes bc prints an answer with only 2 digits after the decimal.
tconvert () {
case $1 in
	[Ff] ) echo -n "$(echo "scale=2 ; ($2 - 32) * 5 / 9" | bc -l) ˚C" ;;
	[cC] ) echo -n "$(echo "scale=2 ; $2 * 9 / 5 + 32" | bc -l) ˚F" ;;
    *    ) echo "Usage: tconvert c|f num"
esac
}


#### Generates a random string of 16 characters by default. Other lengths can be specified by first argument ($1)
rnd () {
if [ -n "$1" ]
	then length=$1
	else length=16
fi
# Apparently, this is more portable.
unset $LC_CTYPE ; LC_CTYPE=C
tr -dc "[:alnum:]$&+^%_" < /dev/urandom | fold -w "${1:-$length}" | head -n 1
}


#### Strips Google tracking bits from Gmail links.
degoogurl () {
	urltool.sh --decode "$1" | sed 's|http://www.google.com/url?q=||g' | sed 's|https://www.google.com/url?q=||g' | sed 's|\&sa=.*||g'
}



###### Usually functions consisting out of non-standard commands (and third party services) below.

#### Upload file to 0x0.st (The Null Pointer) - A file / paste hosting service.
# Usage: 
# nullpointer_upload <file>
# <command> | nullpointer_upload
# More info: https://wiki.archlinux.org/title/List_of_applications#Without_a_dedicated_client
nullpointer_upload () {
[ -n "$1" ] && curl --user-agent "" -F "file=@$1" https://0x0.st || curl --user-agent "" -F "file=@-" https://0x0.st
}


#### ix.io CLI paste hosting service.
# Usage: <command> | ix
ix () { curl --user-agent "" -F 'f:"$1"=<-' ix.io ;}


#### Copying tool using xclip. Puts contents of files into the clipboard (not the selection clipboard)
# Usage: copy < file_to_copy.txt
# Usage: cat file_to_copy.txt | copy
copy () { xclip -sel clip ;}


#### youtube-dl alias. Downloads in h264 1080p max.
# Usage: y https://www.youtube.com/watch?v=dQw4w9WgXcQ
y () { youtube-dl --embed-metadata --embed-subs --embed-chapters --format='bestvideo[height>=720][height<=1080][vcodec^=avc1]+bestaudio/bestvideo+bestaudio/best' "$@" ;}
